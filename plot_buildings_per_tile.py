import matplotlib.pyplot as plt
import csv

with open("buildings_per_tile.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=";")
    values = {row["uuid"]:int(row["count"]) for row in csv_reader}

unique_values = set(values.values())

plt.hist(values.values(), bins=list(unique_values))
plt.show()
