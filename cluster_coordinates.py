from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
import argparse
import csv

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", required=True, help="The input csv file containing the coordinates")
parser.add_argument("-o", "--output", required=True, help="The output csv file containing the assigned cluster indices")
parser.add_argument("-n", "--num-clusters", default=2, type=int, help="The number of clusters")
args = vars(parser.parse_args())

with open(args["input"]) as input_file:
    input_csv = csv.DictReader(input_file, delimiter=";")
    rows = [row for row in input_csv]

coordinates = np.array([(float(row["x"]), float(row["y"])) for row in rows])

kmeans = KMeans(n_clusters=args["num_clusters"])
kmeans.fit(coordinates)

print("Cluster centers: {}".format(kmeans.cluster_centers_))
plt.scatter(x=coordinates[:,0], y=coordinates[:, 1], c=kmeans.labels_, cmap=plt.get_cmap('tab20c'))
plt.show()

for cluster in range(args["num_clusters"]):
    cluster_coordinates = coordinates[np.where(kmeans.labels_ == cluster)]
    print("Cluster {}, bounding box: ({}, {}, {}, {})".format(cluster, min(cluster_coordinates[:, 0]), min(cluster_coordinates[:, 1]), max(cluster_coordinates[:, 0]), max(cluster_coordinates[:, 1])))

with open(args["output"], "w") as output_file:
    csv_writer = csv.DictWriter(output_file, fieldnames=rows[0].keys(), delimiter=";")
    for label, row in zip(kmeans.labels_, rows):
        row["label"] = label
        csv_writer.writerow(row)

