import numpy as np
import cv2
import json
import argparse
import os
import csv


def load_images(annotation_filename):
    with open(annotation_filename) as json_file:
        annotation_dict = json.load(json_file)
        image_dicts = annotation_dict["images"]

        labels = []
        filenames = []
        for image_dict in image_dicts:
            labels.append(1 if image_dict["annotation"] else 0)
            filenames.append(image_dict["filename"])
        return labels, filenames


def label_filename(output_filename):
    path, ext = os.path.splitext(output_filename)
    return "{}_labels{}".format(path, ext)


def write_images(annotation_filename, output_filename, merge_filename):
    labels, all_filenames = load_images(annotation_filename)
    if merge_filename:
        merge_labels, merge_images = load_images(merge_filename)
        labels.extend(merge_labels)
        all_filenames.extend(merge_images)

    images = [cv2.imread(filename) for filename in all_filenames]
    np.save(output_filename, images)
    np.save(label_filename(output_filename), np.array(labels))

def uuid_from(filename):
    return os.path.basename(filename)[:36]

def write_images_csv(annotation_filename, output_filename, merge_filename):
    labels, all_filenames = load_images(annotation_filename)
    if merge_filename:
        merge_labels, merge_images = load_images(merge_filename)
        labels.extend(merge_labels)
        all_filenames.extend(merge_images)

    with open(output_filename, "w") as csv_file:
        csv_writer = csv.DictWriter(csv_file, fieldnames=["uuid", "label"], delimiter=";")
        csv_writer.writeheader()
        for filename, label in zip(all_filenames, labels): 
            csv_writer.writerow({"uuid": uuid_from(filename), "label": label})

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="The input file with the annotations")
    parser.add_argument("-o", "--output", required=True, help="The numpy file with the images to write")
    parser.add_argument("-m", "--merge", default=None, help="The annotation file to merge with the first")
    args = vars(parser.parse_args())
    output_filename = args["output"]
    if output_filename.endswith(".npy"):
        write_images(args["input"],output_filename, args["merge"])
    else:
        write_images_csv(args["input"],output_filename, args["merge"])

if __name__ == "__main__":
    main()
