import argparse
import csv
import matplotlib.pyplot as plt
import numpy as np
from sklearn.calibration import calibration_curve
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-file", required=True, help="A csv file with the model predictions and the labels")
parser.add_argument("-d", "--delimiter", default=";", help="The delimiter for the csv file")
parser.add_argument("-o", "--output-file", required=True, help="The output filename for the plots")
args = parser.parse_args()

predictions_per_set = defaultdict(list)
labels_per_set = defaultdict(list)
with open(args.input_file) as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=args.delimiter)
    for row in csv_reader:
        fold_type = row["fold_type_name"]
        prediction = float(row["prediction"])
        label = int(row["label"])
        predictions_per_set[fold_type].append(prediction)
        labels_per_set[fold_type].append(label)


plt.figure(figsize=(10, 10))
ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2)
ax2 = plt.subplot2grid((3, 1), (2, 0))

ax1.plot([0, 1], [0, 1], "k:", label="Perfectly calibrated")

for fold_type in predictions_per_set.keys():
    predictions = np.array(predictions_per_set[fold_type])
    labels = np.array(labels_per_set[fold_type])
    fraction_of_positives, mean_predicted_value = \
        calibration_curve(labels, predictions, n_bins=10)

    ax1.plot(mean_predicted_value, fraction_of_positives, "s-",
             label="%s" % (fold_type, ))
    ax2.hist(predictions, range=(0, 1), bins=10, label=fold_type,
             histtype="step", lw=2)

ax1.set_ylabel("Fraction of positives")
ax1.set_ylim([-0.05, 1.05])
ax1.legend(loc="lower right")
ax1.set_title('Calibration plots  (reliability curve)')

ax2.set_xlabel("Mean predicted value")
ax2.set_ylabel("Count")
ax2.legend(loc="upper center", ncol=2)

plt.tight_layout()
plt.savefig(args.output_file)
plt.show()

