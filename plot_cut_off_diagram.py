from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from multiprocessing import Pool
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import psycopg2
import psycopg2.extras
import argparse
import sys
import json


def get_annotations(cursor):
    cursor.execute("select annotated_label from beta_product_district.diff_register_predictions_annotations order by tile_id;")
    return np.array([row.annotated_label for row in cursor.fetchall()])

def get_model_predictions(cursor):
    cursor.execute("""select prediction from beta_product_district.model_predictions_geo as mp
                    inner join beta_product_district.diff_register_predictions_annotations as rpa on rpa.tile_id = mp.tile_id
                    order by rpa.tile_id;""")
    return np.array([row.prediction for row in cursor.fetchall()])

def get_building_areas(cursor):
    cursor.execute("select register_label from beta_product_district.diff_register_predictions_annotations order by tile_id;")
    return np.array([row.register_label for row in cursor.fetchall()])

def get_number_of_same_labels(annotations, model_predictions, building_areas, cut_offs):
    model_cut_off, building_area_cut_off = cut_offs
    model_labels = model_predictions >= model_cut_off
    register_labels = building_areas >= building_area_cut_off
    same_labels = np.not_equal(np.equal(annotations, model_labels), np.equal(annotations, register_labels))
    return (model_cut_off, building_area_cut_off, np.sum(same_labels))

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--server-address", default="localhost", help="The ip address of the postgres database")
parser.add_argument("-n", "--db-name", default="postgres", help="The database name in the postgres database")
parser.add_argument("-u", "--db-user", default="postgres", help="The username for the postgres database")
parser.add_argument("-p", "--password", required=True, help="The password for the postgres database")
parser.add_argument("-st", "--number_of_steps", default=20, help="Number of steps to evaluate along the x and y axis")
args = vars(parser.parse_args())

connection_parameters = {
   'host': args["server_address"],
   'database': args["db_name"],
   'user': args["db_user"],
   'password': args["password"]
}

with psycopg2.connect(**connection_parameters) as db_connection:
    with db_connection.cursor(cursor_factory = psycopg2.extras.NamedTupleCursor) as cursor:
        annotations = get_annotations(cursor)
        model_predictions = get_model_predictions(cursor)
        building_areas = get_building_areas(cursor)

        number_of_steps = args["number_of_steps"]
        x_values = np.linspace(0.1, 0.9, number_of_steps)
        y_values = np.linspace(0.2, 0.9, number_of_steps)

        ba_cut_offs, m_cut_offs = np.meshgrid(x_values, y_values)

        grid_values = ((model_cut_off, building_area_cut_off)
                                        for building_area_cut_off in np.ravel(ba_cut_offs)
                                        for model_cut_off in np.ravel(m_cut_offs))

        pool = Pool(processes=4)
        evaluate_grid = partial(get_number_of_same_labels, annotations, model_predictions, building_areas)
        values = pool.map(evaluate_grid, grid_values)
        values = np.array(values)

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        

        x_values = values[:, 0]
        y_values = values[:, 1]
        z_values = values[:, 2]

        # Plot the surface.
        surf = ax.plot_trisurf(x_values, y_values, z_values, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

        plt.show()
           
