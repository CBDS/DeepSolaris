CREATE INDEX idx_tiles_uuid ON tiles(uuid);
CREATE INDEX idx_tiles_tile_id ON tiles(tile_id);
CREATE INDEX idx_tiles_files_tile_id ON tile_files(tile_id);

create table files as
select tf.* from tile_files as tf
inner join tiles as ti on ti.tile_id = tf.tile_id
order by tf.tile_id

alter table files 
add constraint pk_files primary key (file_id);

alter table files 
add constraint fk_tile_files_tile foreign key (tile_id) references tiles(tile_id);

drop table if exists project;
create table project
(
	project_id serial,
	project_name varchar,
	constraint pk_project primary key (project_id)
);

drop table if exists dataset;
create table dataset
(
	dataset_id serial,
	dataset_name varchar,
	constraint pk_dataset_id primary key (dataset_id)
);

drop table if exists dataset_files_import;
create table dataset_files_import
(
	import_id int,
	dataset_name varchar,
	layername varchar,
	uuid_string varchar
);

drop table if exists dataset_files;
create table dataset_files
(
	dataset_id int,
	file_id int,
	constraint pk_dataset_files primary key (dataset_id, file_id),
	constraint fk_dataset_files_dataset_id foreign key (dataset_id) references dataset(dataset_id),
	constraint fk_dataset_files_file_id foreign key (file_id) references files(file_id)
);

drop table if exists project_dataset;
create table project_dataset
(
	project_id int,
	dataset_id int,
	constraint pk_project_dataset primary key (project_id, dataset_id),
	constraint fk_project_dataset_project_id foreign key (project_id) references project(project_id),
	constraint fk_project_dataset_dataset_id foreign key (dataset_id) references dataset(dataset_id)
);

drop table if exists dataset_fold;
create table dataset_fold
(
	fold_id serial,
	fold_name varchar,
	dataset_id int,
	training_set_percentage double precision,
	test_set_percentage double precision,
	validation_set_percentage double precision,
	constraint pk_dataset_folds primary key (fold_id),
	constraint fk_dataset_folds_dataset_id foreign key (dataset_id) references dataset(dataset_id)
);

drop table if exists dataset_fold_types;
create table dataset_fold_types
(
	fold_type_id int,
	fold_type_name varchar,
	constraint pk_fold_type_id primary key (fold_type_id)
);

insert into dataset_fold_types (fold_type_id, fold_type_name) 
values (1, 'training_set'), (2, 'test_set'), (3, 'validation_set');

drop table if exists dataset_fold_files;
create table dataset_fold_files
(
	dataset_id int,
	fold_id int,	
	file_id int,	
	fold_type_id int,
	constraint pk_dataset_fold_tiles primary key (fold_id, file_id), -- add dataset_id?
	constraint fk_fold_id foreign key (fold_id) references dataset_fold(fold_id),
	constraint fk_dataset_files foreign key (dataset_id, file_id) references dataset_files(dataset_id, file_id),
	constraint fk_fold_type_id foreign key (fold_type_id) references dataset_fold_types(fold_type_id)	
);

drop table if exists models;
create table models
(
	model_id serial,
	model_name varchar,
	model_filename varchar,
	constraint pk_models primary key (model_id)
);

drop table if exists model_predictions_import;
create table model_predictions_import
(
	import_id int,
	uuid_string varchar(36),   
    prediction double precision,
	model_id int,
	fold_id int
);


drop table if exists model_predictions;
create table model_predictions
(
	prediction_id serial,
	prediction double precision,
	model_id int,
	fold_id int,
	file_id int,
	constraint pk_model_predictions primary key (prediction_id),
	constraint fk_model_id foreign key (model_id) references models(model_id),
	constraint fk_model_predictions_dataset_fold_files_id foreign key (fold_id, file_id) references dataset_fold_files(fold_id, file_id)
);

drop table if exists files_annotations;
create table files_annotations
(
	annotation_id serial,
	dataset_id int,
	file_id int,
	label int, 
	num_positives int, 
	num_negatives int, 
	num_dkn int,
	total_annotations int,
	constraint pk_files_annotations primary key (annotation_id),
	constraint fk_dataset_files foreign key (dataset_id, file_id) references dataset_files (dataset_id, file_id)
);

insert into files_annotations (dataset_id, file_id, label, num_positives, num_negatives, num_dkn, total_annotations)
select dfi.dataset_id, dfi.file_id, apt.label, apt.num_positives, apt.num_negatives, apt.num_dkn, apt.total_annotations  from dataset as d
inner join dataset_files as dfi on d.dataset_id = dfi.dataset_id
inner join files as f on f.file_id = dfi.file_id
inner join tiles as ti on ti.tile_id = f.tile_id
inner join annotations_per_tile as apt on apt.uuid = uuid(ti.uuid)

create function get_new_import_id()
returns integer
language sql
as $$
	select 
		case when max(import_id) is null then 0 else max(import_id) end + 1 as import_id 	
	from model_predictions_import; 
$$;

create function get_new_data_files_import_id()
returns integer
language sql
as $$
	select 
		case when max(import_id) is null then 0 else max(import_id) end + 1 as import_id 	
	from dataset_files_import; 
$$;

create or replace function copy_model_predictions(import_id int)
returns integer
language sql
as $$
insert into model_predictions (prediction, model_id, fold_id, file_id)
select mpi.prediction, mpi.model_id, dfo.fold_id, dfi.file_id
from dataset_fold as dfo 
inner join dataset_fold_files as dfi on dfi.fold_id = dfo.fold_id --and dfi.dataset_id = dfo.dataset_id
inner join files as f on f.file_id = dfi.file_id
inner join tiles as ti on ti.tile_id = f.tile_id
inner join model_predictions_import as mpi on mpi.fold_id = dfi.fold_id and mpi.uuid_string = ti.uuid
where mpi.import_id = import_id;
delete from model_predictions_import as mpi where mpi.import_id = import_id returning import_id;
$$;

create or replace function copy_dataset_files(import_id int)
returns integer
language sql
as $$
insert into dataset_files (dataset_id, file_id)
select d.dataset_id, tf.file_id from dataset_files_import as dfi
inner join dataset as d on d.dataset_name = dfi.dataset_name
inner join files as tf on tf.layername = dfi.layername
inner join tiles as ti on ti.tile_id = tf.tile_id and ti.uuid = dfi.uuid_string;
delete from dataset_files_import as dfi where dfi.import_id = import_id returning import_id;
$$;
