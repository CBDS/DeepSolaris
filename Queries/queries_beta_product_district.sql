--Create a separate schema for the beta product to keep the other database clean
create schema if not exists beta_product_district;

-- drop beta product tables in public schema
drop table if exists public.annotations_per_tile_geo;
drop table if exists public.building_to_tile;
drop table if exists public.model_predictions;
drop table if exists public.model_predictions_geo;
drop table if exists public.district_to_tile;
drop table if exists public.district_to_tile_building;
drop table if exists public.perc_solar_panel_differences_per_di;
drop table if exists public.pv_2017_nl_new;
drop table if exists public.register_label_per_building;
drop table if exists public.selected_buildings;
drop table if exists public.selected_districts;
drop table if exists public.selected_tiles;
drop table if exists public.weighted_annotations_aggr_di;
drop table if exists public.weighted_annotations_per_di;
drop table if exists public.weighted_diff_per_di;
drop table if exists public.weighted_diff_per_di_selection;
drop table if exists public.weighted_predictions_aggr_di;
drop table if exists public.weighted_predictions_per_di;
drop table if exists public.weighted_register_labels_aggr_di;

--Create a table with selected districts
drop table if exists beta_product_district.selected_districts;
create table beta_product_district.selected_districts
as
select
    di.*
from public.wijk_2017 as di
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), di.wkb_geometry);

alter table beta_product_district.selected_districts
add column in_train_set boolean;

alter table beta_product_district.selected_districts
add column in_validation_set boolean;

alter table beta_product_district.selected_districts
add column in_train_validation_set boolean;

update beta_product_district.selected_districts
set in_train_set = ST_Intersects(wkb_geometry, ai.area)
from public.areaofinterest as ai
where ai.area_id = 19 and ST_Intersects(wkb_geometry, ai.area);

update beta_product_district.selected_districts
set in_validation_set = ST_Intersects(wkb_geometry, ai.area)
from public.areaofinterest as ai
where ai.area_id = 23 and ST_Intersects(wkb_geometry, ai.area);

update beta_product_district.selected_districts
set in_train_validation_set = in_train_set or in_validation_set;

CREATE INDEX "selected_districts_geom_index" ON beta_product_district.selected_districts USING gist (wkb_geometry);

--Create a table beta_product_district.with selected buildings
drop table if exists beta_product_district.selected_buildings;
create table beta_product_district.selected_buildings
as
(
    select * from bagactueel.pand as pa
    where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak) and
    pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
    (pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and
    pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and
    pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus)
);

CREATE INDEX "selected_buildings_geom_index" ON beta_product_district.selected_buildings USING gist (geovlak);

select count(*) from beta_product_district.selected_buildings;
select count(distinct(identificatie)) from beta_product_district.selected_buildings;


-- Create table with selected tiles
-- changed this to only include tiles that intersect with buildings
-- create table containing the annotated tiles that are contain buildings and the tiles that are inside a district;
-- some tiles were located in Germany.
drop table if exists beta_product_district.selected_tiles;
create table beta_product_district.selected_tiles
as
select ti.* from public.tiles as ti
inner join (
	select distinct(tile_id) from public.tiles as ti 
	inner join beta_product_district.selected_buildings as sb on ST_Intersects(sb.geovlak, ti.area)
	inner join beta_product_district.selected_districts as sn on ST_Intersects(ti.area, sn.wkb_geometry)
	where ti.area_id = 19 or ti.area_id = 23
) st on st.tile_id = ti.tile_id;

CREATE INDEX "selected_tiles_geom_index" ON beta_product_district.selected_tiles USING gist (area);

-- Assign tiles to district
-- 118682 items?
drop table if exists beta_product_district.district_to_tile;
create table beta_product_district.district_to_tile
as
select *,
    ST_Area(ST_Intersection(ti.area, di.wkb_geometry)) / ST_Area(ti.area) as area_fraction_in_district
from beta_product_district.selected_districts as di
inner join beta_product_district.selected_tiles as ti on ST_Intersects(di.wkb_geometry, ti.area)
where in_train_validation_set = true;

CREATE INDEX "district_to_tile_area_index" ON beta_product_district.district_to_tile USING gist (area);
CREATE INDEX "district_to_tile_wkb_geometry_index" ON beta_product_district.district_to_tile USING gist (wkb_geometry);

-- Create entropy function
create or replace function entropy(
	number_positives double precision, 
	number_negatives double precision 
	) 
returns double precision AS $$
declare
	total double precision;
	p_positive double precision;
	p_negative double precision;	
begin
	total := number_positives + number_negatives;	
	p_positive := number_positives / total; -- p
	p_negative := number_negatives / total; -- 1 - p
	
	if p_positive = 0 or p_negative = 0 then
		return log(1) / log(2);		
	end if;
		
   	return -p_positive * (log(p_positive) / log(2)) - p_negative * (log(p_negative) / log(2));
end;
$$ language plpgsql;

create or replace function gini(
	number_positives double precision, 
	number_negatives double precision 
	) 
returns double precision AS $$
declare
	total double precision;
	p_positive double precision;
	p_negative double precision;	
begin
	total := number_positives + number_negatives;	
	p_positive := number_positives / total; -- p
	p_negative := number_negatives / total; -- 1 - p

   	return 1.0 - (p_positive * p_positive + p_negative * p_negative);
end;
$$ language plpgsql;

-- Create a table beta_product_district.with the annotations and the geo information per tile
-- Only use the tiles from table selected_tiles that are in the area of interest.
-- Only use annotations that have a label 0 (= does not contain solar panels) or 1 (=contains solar panels)

drop table if exists beta_product_district.annotations_per_tile_geo;
create table beta_product_district.annotations_per_tile_geo
as
select 
	at.*, ti.tile_id, ti.area as tile_geom, ti.area_id,
	entropy(num_positives, num_negatives) as entropy,
	gini(num_positives, num_negatives) as gini	
from public.annotations_per_tile as at
inner join beta_product_district.selected_tiles as ti on UUID(ti.uuid) = at.uuid  
where label >= 0 and num_positives != num_negatives;

alter table beta_product_district.annotations_per_tile_geo
add constraint pk_annotations_per_tile_geo primary key (id);

CREATE INDEX "annotations_per_tile_geo_tile_geom_index" ON beta_product_district.annotations_per_tile_geo USING gist (tile_geom);

--544941
drop table if exists beta_product_district.solar_panel_addresses;
create table beta_product_district.solar_panel_addresses
as
select
	pv.*,
    af.pandid,
	af.adresseerbaarobject, 
	af.geopunt as location
from bagactueel.adres_full as af
inner join (
    select postcode, number, number_add, building_id,
        min(year_in_use) as year_in_use_first,
        max(year_in_use) as year_in_use_last,
        min(date_in_use) as date_in_use_first,
        max(date_in_use) as date_in_use_last,
        count(*) as number_of_register_entries
    from public.solarpanel_addresses_orig as so
    where length(trim(building_id)) > 0
    group by postcode, number, number_add, building_id
    ) pv on af.adresseerbaarobject = pv.building_id;

-- no duplicate addresses!
select building_id, count(*) from beta_product_district.unique_solar_panel_addresses
group by building_id
having count(*) > 1;

select count(*) from beta_product_district.unique_solar_panel_addresses;

--544689
drop table if exists beta_product_district.solarpanel_addresses_buildings;
create table beta_product_district.solarpanel_addresses_buildings
as
select
    af.*,
    pa.gid as pand_id, pa.geovlak as building_polygon
from beta_product_district.solar_panel_addresses as af
inner join bagactueel.pand as pa on pa.identificatie = af.pandid and   
    pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
    pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and
    pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and
    pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus;

-- we are interested in unique buildings
drop table if exists beta_product_district.pv_2017_nl_ub;
create table beta_product_district.pv_2017_nl_ub
as
select
	min(af.postcode) as postcode, 
	min(af.number) as number, 
	min(af.number_add) as number_add, 
	min(af.building_id) as address_id,
    min(year_in_use_first) as year_in_use_first,
    max(year_in_use_last) as year_in_use_last,
    min(date_in_use_first) as date_in_use_first,
    max(date_in_use_last) as date_in_use_last,
    min(af.pand_id) as pand_id, 
	min(af.building_polygon) as building_polygon,	
	min(af.adresseerbaarobject) as adresseerbaarobject, 
	min(af.location) as location
from beta_product_district.solarpanel_addresses_buildings as af
group by pand_id;

-- 529273
select count(*) from beta_product_district.pv_2017_nl_ub;

-- we are interested in unique addresses; only one building per address
-- 528477 --> 529273 - 528477 = 796 buildings that have been assigned to an address that is already in the db.
drop table if exists beta_product_district.pv_2017_nl_new;
create table beta_product_district.pv_2017_nl_new
as
select
	min(af.postcode) as postcode, 
	min(af.number) as number, 
	min(af.number_add) as number_add, 
	min(af.address_id) as address_id,
    min(year_in_use_first) as year_in_use_first,
    max(year_in_use_last) as year_in_use_last,
    min(date_in_use_first) as date_in_use_first,
    max(date_in_use_last) as date_in_use_last,
    min(af.pand_id) as pand_id, 
	min(af.building_polygon) as building_polygon,	
	min(af.adresseerbaarobject) as adresseerbaarobject, 
	min(af.location) as location
from beta_product_district.pv_2017_nl_ub as af
group by address_id;

-- why multiple items here? multiple buildings for one address.
-- we are interested in unique buildings, do we need to 
-- 796 addresses with more than one building
select address_id, min(pand_id), max(pand_id), count(*)
from beta_product_district.pv_2017_nl_ub
group by address_id
having count(*) > 1;

-- 796 duplicate addresses
select sum(number_of_duplicates-1)
from (
	select count(*) as number_of_duplicates
	from beta_product_district.pv_2017_nl_ub
	group by address_id
	having count(*) > 1
) counts;


select pand_id, count(*) 
from beta_product_district.pv_2017_nl_new
group by pand_id
having count(*) > 1;

-- here
-- Assign buildings to district
-- 347856 buildings
drop table if exists beta_product_district.register_label_per_building;
create table beta_product_district.register_label_per_building
as
(
    select *,
        case when pv.pand_id is not null then 1 else 0 end as register_label   
    from beta_product_district.selected_buildings as pa
    left join beta_product_district.pv_2017_nl_new as pv on pv.pand_id = pa.gid
);

CREATE INDEX "register_label_per_building_geovlak_index" ON beta_product_district.register_label_per_building USING gist (geovlak);

-- 347856 buildings
select count(*) from beta_product_district.register_label_per_building;

select count(distinct(gid)) from beta_product_district.register_label_per_building;

select sum(number_of_duplicates) from
(select 
	count(*) as number_of_duplicates
from beta_product_district.register_label_per_building
group by address_id
) counts;


-- Assign buildings to tile
-- 117708 unique buildings, but duplicates because we are assigning them to a tile
-- 291530 tile, building combinations.
drop table if exists beta_product_district.building_to_tile;
create table beta_product_district.building_to_tile
as
select *,
    ST_Area(ST_Intersection(ti.area, sb.geovlak)) / ST_Area(ti.area) as building_area_fraction_in_tile    
from beta_product_district.register_label_per_building as sb
inner join beta_product_district.selected_tiles as ti on ST_Intersects(sb.geovlak, ti.area);

-- building present multiple times because of different tiles.
select * from beta_product_district.building_to_tile
where identificatie = '0994100002043063'
order by number, number_add, tile_id;

select * from beta_product_district.register_label_per_building as pa
where identificatie = '0994100002043063';

select * 
from beta_product_district.selected_buildings as pa
where gid = 13870737;

select * from beta_product_district.pv_2017_nl_new
where pand = 13870737



CREATE INDEX "building_to_tile_geovlak_index" ON beta_product_district.building_to_tile USING gist (geovlak);
CREATE INDEX "building_to_tile_area_index" ON beta_product_district.building_to_tile USING gist (area);

drop table if exists beta_product_district.district_to_tile_building;
create table beta_product_district.district_to_tile_building
as
select bt.*,
    di.wk_code, di.wk_naam, di.gm_code, di.gm_naam,
	ST_Area(ST_Intersection(bt.area, di.wkb_geometry)) / ST_Area(bt.area) as tile_area_fraction_in_district,
	di.wkb_geometry
from beta_product_district.selected_districts as di
inner join beta_product_district.building_to_tile as bt on ST_Intersects(di.wkb_geometry, bt.area);

CREATE INDEX "district_to_tile_building_geovlak_index" ON beta_product_district.district_to_tile_building USING gist (geovlak);
CREATE INDEX "district_to_tile_building_area_index" ON beta_product_district.district_to_tile_building USING gist (area);
CREATE INDEX "district_to_tile_building_wkb_geometry_index" ON beta_product_district.district_to_tile_building USING gist (wkb_geometry);

-- Create weighing table beta_product_district.for annotations
drop table if exists beta_product_district.weighted_annotations_per_di;
create table beta_product_district.weighted_annotations_per_di
as
select
    ag.*, 
    nt.wk_code, nt.wk_naam, nt.gm_code, nt.gm_naam, nt.area_fraction_in_district    
from beta_product_district.annotations_per_tile_geo as ag
inner join beta_product_district.district_to_tile as nt on nt.tile_id = ag.tile_id;

alter table beta_product_district.weighted_annotations_per_di
add constraint pk_weighted_annotations_per_di
primary key (wk_code, tile_id);

-- Number of annotations/annotated solar panels per district in Zuid Limburg
drop table if exists beta_product_district.weighted_annotations_aggr_di;
create table beta_product_district.weighted_annotations_aggr_di
as
select
    di.wk_code, di.wk_naam, di.gm_naam,
    count(distinct(id)) as num_annotations,    
    sum(coalesce(area_fraction_in_district, 0)) as number_of_tiles_annotated,
    sum(
        case 
			when wa.label != 1 then 0
			when wa.label = 1 then 1 * wa.area_fraction_in_district		
        end
    )  as num_solar_panels_annotations,
	sum(
        case 
			when wa.label != 0 then 0
			when wa.label = 0 then 1 * wa.area_fraction_in_district		
        end
    ) as num_negative_annotations,
    wkb_geometry
from beta_product_district.selected_districts as di
left join beta_product_district.weighted_annotations_per_di as wa on di.wk_code = wa.wk_code
group by di.wk_code, di.wk_naam, di.gm_naam, wkb_geometry;

-- Create weighing table beta_product_district.for model predictions
drop table if exists beta_product_district.model_predictions;
create table beta_product_district.model_predictions
(      
    uuid_string varchar(36),   
    prediction double precision,
	label int
);

-- import does not work for some reason
-- import with import wizard pgadmin 4
-- copy beta_product_district.model_predictions (uuid_string, prediction, label)
-- from '/media/tdjg/Data1/DeepSolaris/all_predictions.csv'
-- csv delimiter ';' header;

alter table beta_product_district.model_predictions
add column prediction_id serial;

alter table beta_product_district.model_predictions
add column uuid UUID;

alter table beta_product_district.model_predictions
add column model_name varchar;

delete from beta_product_district.model_predictions
where uuid_string like 'feh%';

update beta_product_district.model_predictions
set uuid = UUID(uuid_string);

update beta_product_district.model_predictions
set model_name = 'vgg16_best';

alter table beta_product_district.model_predictions
drop column uuid_string;

alter table beta_product_district.model_predictions
drop column label;

delete from beta_product_district.model_predictions
where prediction_id in
(
    select max(prediction_id) from beta_product_district.model_predictions
    group by uuid
    having count(uuid) > 1
);

alter table beta_product_district.model_predictions
add constraint pk_model_predictions primary key (uuid, model_name);

drop table if exists beta_product_district.model_predictions_geo;
create table beta_product_district.model_predictions_geo
as
select mp.*, ti.tile_id, ti.area_id, ti.area as tile_geom
from beta_product_district.model_predictions as mp
inner join beta_product_district.selected_tiles as ti on UUID(ti.uuid) = mp.uuid;

alter table beta_product_district.model_predictions_geo
add constraint pk_model_predictions_geo
primary key  (tile_id);

--drop table if exists beta_product_district.model_predictions_geo;
--create table beta_product_district.model_predictions_geo
--as
--select * from beta_product.model_predictions_geo;

create index model_predictions_geo_idx
on beta_product_district.model_predictions_geo
using gist(tile_geom);

drop table if exists beta_product_district.weighted_predictions_per_di;
create table beta_product_district.weighted_predictions_per_di
as
select
    mp.*, 
    nt.wk_code, nt.wk_naam, nt.gm_code, nt.gm_naam, nt.area_fraction_in_district    
from beta_product_district.model_predictions_geo as mp
inner join beta_product_district.district_to_tile as nt on nt.tile_id = mp.tile_id;

alter table beta_product_district.weighted_predictions_per_di
add constraint pk_weighted_predictions_per_di
primary key (wk_code, tile_id);

-- Model predictions per district
drop table if exists beta_product_district.weighted_predictions_aggr_di;
create table beta_product_district.weighted_predictions_aggr_di
as
select
    di.wk_code, di.wk_naam, di.gm_naam,
    count(distinct(prediction_id)) as num_predictions,
    sum(coalesce(area_fraction_in_district, 0)) as weighted_num_predictions,
    sum(
        case 
			when wp.label != 1 then 0
			when wp.label = 1 then 1 * wp.area_fraction_in_district		
        end
    )  as num_solar_panels_predictions,
	sum(
        case 
			when wp.label != 0 then 0
			when wp.label = 0 then 1 * wp.area_fraction_in_district		
        end
    ) as num_negative_predictions,
    wkb_geometry
from beta_product_district.selected_districts as di
inner join beta_product_district.weighted_predictions_per_di as wp on di.wk_code = wp.wk_code
group by di.wk_code, di.wk_naam, di.gm_naam, wkb_geometry;

-- Create weighing table beta_product_district.for register labels per district
-- districts

drop table if exists beta_product_district.weighted_register_labels_aggr_di;
create table beta_product_district.weighted_register_labels_aggr_di
as
select
    di.wk_code, di.wk_naam, di.gm_naam,
    count(*) as num_register_labels,
	sum(coalesce(tile_area_fraction_in_district, 0)) as weighted_tiles_district,
   	sum(coalesce(tile_area_fraction_in_district * building_area_fraction_in_tile, 0)) as weighted_number_register_labels,
    	sum(
			case 
				when register_label != 1 then 0
				when register_label = 1 then 1 * tile_area_fraction_in_district * building_area_fraction_in_tile		
			end
		)  as num_solar_panels_register,
		sum(
			case 
				when register_label != 0 then 0
				when register_label = 0 then 1 * tile_area_fraction_in_district * building_area_fraction_in_tile	
			end
		) as num_negative_register_labels,
     wkb_geometry
from beta_product_district.district_to_tile_building as di
group by di.wk_code, di.wk_naam, di.gm_naam, di.wkb_geometry;

CREATE INDEX "weighted_register_labels_aggr_di_wkb_geometry_index" ON beta_product_district.weighted_register_labels_aggr_di USING gist (wkb_geometry);


-- Create diff tables per district
-- We need differences between labels!! Not just in numbers.
-- should num_predictions, num_annotations, num_register_labels be weighted?
-- 208 districts.

-- Diff predictions - annotations on annotated tiles
drop table if exists beta_product_district.diff_predictions_annotations;
create table beta_product_district.diff_predictions_annotations
as
select
	atg.tile_id, atg.tile_geom, 
	atg.total_annotations, atg.num_positives, atg.num_negatives, atg.default_annotation,
	atg.area_id, atg.uuid, mp.model_name,
	atg.label as annotated_label, 
	mp.label as predicted_label,
	-- indicate whether tiles is TP = 0, TN = 1, FN = 2, FP = 3
	case 
		when atg.label = 1 and mp.label = 1 then 0  
		when atg.label = 0 and mp.label = 0 then 1 
		when atg.label = 1 and mp.label = 0 then 2 
		when atg.label = 0 and mp.label = 1 then 3 
	end as predictions_confusion_matrix_category	
from beta_product_district.annotations_per_tile_geo as atg
inner join beta_product_district.model_predictions_geo as mp on mp.tile_id = atg.tile_id;

select 
	sum(case when annotated_label = 1 and predicted_label = 0 then 1 else 0 end) as false_negatives,
	sum(case when annotated_label = 0 and predicted_label = 1 then 1 else 0 end) as false_positives
from beta_product_district.diff_predictions_annotations
where predicted_label <> annotated_label;

-- Diff register - annotations on annotated tiles
-- Here we assign a register label based on the weighted label
-- As register labels are weighted by the area of the building, we assume that at least three panels
-- need to be visible to be able to be counted as label 1. Each solar panel is 1x1.5m approximately or
-- 10x15pixels = 150 pixels in total, 3 solar panels is 450 pixels, is 0.01 (1%) of the total area of the tile.
drop table if exists beta_product_district.building_to_tile_aggr;
create table beta_product_district.building_to_tile_aggr
as
select 
	tile_id, 
	sum(coalesce(building_area_fraction_in_tile, 0)) as weighted_number_register_labels_tile,
	sum(
		case 
			when register_label != 1 then 0
			when register_label = 1 then 1 * building_area_fraction_in_tile		
		end
	)  as num_solar_panels_register_tile,
	sum(
		case 
			when register_label != 0 then 0
			when register_label = 0 then 1 * building_area_fraction_in_tile	
		end
	) as num_negative_register_labels_tile,	
	--case 
	--	when sum(register_label * building_area_fraction_in_tile) < 0.07 then 0
	--	else 1
	--end as register_label,
	sum(register_label * building_area_fraction_in_tile) as register_label,
	
	count(*) as num_buildings,
	bt.area
from beta_product_district.building_to_tile as bt
group by bt.tile_id, bt.area;

select count(distinct(tile_id))
from beta_product_district.building_to_tile as bt

select 
	sum(building_area_fraction_in_tile)
from beta_product_district.building_to_tile as bt
group by bt.tile_id
having sum(building_area_fraction_in_tile) < 0; 

select
	sum(register_label * building_area_fraction_in_tile) as weighted_label,
	count(*) as num_buildings
from beta_product_district.building_to_tile as bt
group by bt.tile_id, bt.area
order by 1 desc;

select uuid, * from beta_product_district.building_to_tile as bt
where bt.tile_id in
(
	select bt.tile_id from beta_product_district.building_to_tile as bt
	group by uuid, bt.tile_id
	having count(*) > 20
)
order by uuid, identificatie;

select count(*) from bagactueel.pandactueel
where identificatie = '0994100002043063';

copy
(
	select
		uuid,
		count(*)
	from beta_product_district.building_to_tile as bt
	group by uuid, bt.tile_id
	order by count(*) desc
) to '/tmp/buildings_per_tile.csv'
csv header delimiter ';';
-- 116666
select count(distinct(tile_id)) from beta_product_district.building_to_tile_aggr;

select 
	min(register_label), max(register_label) 
from building_to_tile_aggr;


drop table if exists beta_product_district.diff_register_annotations;
create table beta_product_district.diff_register_annotations
as
select
	atg.tile_id, atg.tile_geom, 
	atg.total_annotations, atg.num_positives, atg.num_negatives, atg.default_annotation,
	atg.area_id, atg.uuid, 
	atg.label as annotated_label, 
	rp.register_label as register_label, 
	rp.weighted_number_register_labels_tile,
	rp.num_solar_panels_register_tile, 
	rp.num_negative_register_labels_tile,
	-- indicate whether tiles is TP = 0, TN = 1, FN = 2, FP = 3
	case 
		when atg.label = 1 and rp.register_label = 1 then 0  
		when atg.label = 0 and rp.register_label = 0 then 1 
		when atg.label = 1 and rp.register_label = 0 then 2 
		when atg.label = 0 and rp.register_label = 1 then 3 
	end as register_confusion_matrix_category
from beta_product_district.annotations_per_tile_geo as atg
inner join beta_product_district.building_to_tile_aggr as rp on rp.tile_id = atg.tile_id;



select 
	sum(case when annotated_label = 1 and register_label = 0 then 1 else 0 end) as false_negatives,
	sum(case when annotated_label = 0 and register_label = 1 then 1 else 0 end) as false_positives
from beta_product_district.diff_register_annotations
where register_label <> annotated_label;


drop table if exists beta_product_district.diff_register_predictions_annotations;
create table beta_product_district.diff_register_predictions_annotations
as
select 
	da.tile_id, da.tile_geom, 
	da.total_annotations, da.num_positives, da.num_negatives, da.default_annotation,
	da.area_id, da.uuid, 
	da.annotated_label as annotated_label,
	da.predicted_label as predicted_label,
	ra.register_label as register_label, 
	ra.weighted_number_register_labels_tile,
	ra.num_solar_panels_register_tile, 
	ra.num_negative_register_labels_tile,
	predictions_confusion_matrix_category,
	register_confusion_matrix_category
from beta_product_district.diff_predictions_annotations as da
inner join beta_product_district.diff_register_annotations as ra on ra.tile_id = da.tile_id;

select 
	count(*), 
	sum(annotated_label) as annotated_sp, 
	sum(predicted_label) as predicted_sp,
	sum(register_label) as register_sp
from beta_product_district.diff_register_predictions_annotations
where tile_id in (
select tile_id from beta_product_district.district_to_tile
where wk_naam = 'De Hemelder'
);

-- Check
select 
	num_positives, num_negatives, total_annotations, 
	annotated_label, predicted_label, register_label,
	weighted_number_register_labels_tile, num_solar_panels_register_tile, num_negative_register_labels_tile	
from beta_product_district.diff_register_predictions_annotations 
where 
	annotated_label = 1 and register_label = 0 and num_solar_panels_register_tile > 0
order by num_solar_panels_register_tile asc;

-- 53150
select count(distinct(tile_id)) from beta_product_district.diff_register_predictions_annotations;

-- Create MCC function
create or replace function MCC(
	tp double precision, 
	tn double precision, 
	fn double precision, 
	fp double precision) 
returns double precision AS $$
declare
	d double precision;
begin
	d := (tp+fp)*(tp+fn)*(tn+fp)*(tn+fn);
	if d = 0 then 
		d = 1; 
	end if;
   	return (tp * tn - fp * fn) / sqrt(d);
end;
$$ language plpgsql;

-- register, predictions, annotations for the annotations tiles per district.
-- NOTE: number of register labels correct? should be number of tiles with negatives/positives too
drop table if exists beta_product_district.diff_register_predictions_annotations_di;
create table beta_product_district.diff_register_predictions_annotations_di
as
select 
	wk_code,
	wk_naam,
	gm_naam,
	wkb_geometry,
	weighted_tiles_district,
	weighted_number_of_annotations,
	weighted_number_of_register_labels,
	
	num_positive_annotations,
	num_negative_annotations,
	num_positive_annotations / weighted_tiles_district as perc_positve_annotations, 
	num_negative_annotations / weighted_tiles_district as perc_negative_annotations,
	
	num_positive_predictions,
	num_negative_predictions,
	num_positive_predictions / weighted_tiles_district as perc_positive_predictions,
	num_negative_predictions / weighted_tiles_district as perc_negative_predictions,
		
	num_positive_register_labels,	
	num_negative_register_labels,	
	num_positive_register_labels / weighted_tiles_district as perc_positive_register_labels,
	num_negative_register_labels / weighted_tiles_district as perc_negative_register_labels,	
	
	number_of_predicted_true_positives,
	number_of_predicted_true_negatives,
	number_of_predicted_false_negatives,
	number_of_predicted_false_positives,
	MCC(
		number_of_predicted_true_positives,
		number_of_predicted_true_negatives,
		number_of_predicted_false_negatives,
		number_of_predicted_false_positives
	) as mcc_predictions,
	
	number_of_register_true_positives,
	number_of_register_true_negatives,
	number_of_register_false_negatives,
	number_of_register_false_positives,
	MCC(
		number_of_register_true_positives,
		number_of_register_true_negatives,
		number_of_register_false_negatives,
		number_of_register_false_positives
	) as mcc_register_labels
from 
(
select 
	nt.wk_code,
	nt.wk_naam,
	nt.gm_naam,
	nt.wkb_geometry,
	sum(coalesce(area_fraction_in_district, 0)) as weighted_tiles_district,
	sum(
		case 
			when annotated_label != 1 then 0
			when annotated_label = 1 then 1 * area_fraction_in_district		
		end
	)  as num_positive_annotations,
	sum(
		case 
			when annotated_label != 0 then 0
			when annotated_label = 0 then 1 * area_fraction_in_district	
		end
	) as num_negative_annotations,	
	
	
	sum(coalesce(area_fraction_in_district, 0)) as weighted_number_of_annotations,
	sum(
		case 
			when predicted_label != 1 then 0
			when predicted_label = 1 then 1 * area_fraction_in_district		
		end
	)  as num_positive_predictions,
	sum(
		case 
			when predicted_label != 0 then 0
			when predicted_label = 0 then 1 * area_fraction_in_district	
		end
	) as num_negative_predictions,
	
	sum(coalesce(area_fraction_in_district, 0)) as weighted_number_of_register_labels,
	sum(
		case 
			when register_label != 1 then 0
			when register_label = 1 then 1 * area_fraction_in_district		
		end
	)  as num_positive_register_labels,
	sum(
		case 
			when register_label != 0 then 0
			when register_label = 0 then 1 * area_fraction_in_district	
		end
	) as num_negative_register_labels,
	-- calculate confusion matrix per district where TP = 0, TN = 1, FN = 2, FP = 3
	-- we use the annotated labels as ground truth
	sum(case when predictions_confusion_matrix_category = 0 then 1 * area_fraction_in_district else 0 end) as number_of_predicted_true_positives,
	sum(case when predictions_confusion_matrix_category = 1 then 1 * area_fraction_in_district else 0 end) as number_of_predicted_true_negatives,
	sum(case when predictions_confusion_matrix_category = 2 then 1 * area_fraction_in_district else 0 end) as number_of_predicted_false_negatives,
	sum(case when predictions_confusion_matrix_category = 3 then 1 * area_fraction_in_district else 0 end) as number_of_predicted_false_positives,
	
	sum(case when register_confusion_matrix_category = 0 then 1 * area_fraction_in_district else 0 end) as number_of_register_true_positives,
	sum(case when register_confusion_matrix_category = 1 then 1 * area_fraction_in_district else 0 end) as number_of_register_true_negatives,
	sum(case when register_confusion_matrix_category = 2 then 1 * area_fraction_in_district else 0 end) as number_of_register_false_negatives,
	sum(case when register_confusion_matrix_category = 3 then 1 * area_fraction_in_district else 0 end) as number_of_register_false_positives	
	
from beta_product_district.diff_register_predictions_annotations as dr
inner join beta_product_district.district_to_tile as nt on nt.tile_id = dr.tile_id
group by nt.wk_code, nt.wk_naam, nt.gm_naam, nt.wkb_geometry
) s;

drop table if exists beta_product_district.diff_register_predictions_annotations_di_selection;
create table beta_product_district.diff_register_predictions_annotations_di_selection
as
select
    wk_code, wk_naam, gm_naam,
    min(mcc_predictions) over() as min_mcc_predictions,
    max(mcc_predictions) over() as max_mcc_predictions,
    mcc_predictions,
    min(mcc_register_labels) over() as min_mcc_register_labels,
    max(mcc_register_labels) over() as max_mcc_register_labels,	  
	mcc_register_labels,
    wkb_geometry
from beta_product_district.diff_register_predictions_annotations_di;

CREATE INDEX "diff_register_predictions_annotations_di_selection_wkb_geometry_index" ON beta_product_district.diff_register_predictions_annotations_di_selection USING gist (wkb_geometry);

select
	wk_code, wk_naam, gm_naam,
	mcc_predictions,
	mcc_register_labels,
	mcc_register_labels - mcc_predictions as diff_register_predictions	
from beta_product_district.diff_register_predictions_annotations_di
order by mcc_register_labels - mcc_predictions asc

select * from beta_product_district.diff_register_predictions_annotations_di;

select 
	sum(case when annotated_label = predicted_label then 1 else 0 end) as register_wrong,
	sum(case when annotated_label = register_label then 1 else 0 end) as predictions_wrong,
	sum(case when predicted_label = register_label then 1 else 0 end) as annotation_maybe_wrong
from beta_product_district.diff_register_predictions_annotations
where annotated_label <> predicted_label or annotated_label <> register_label;

-- correct labels
-- 53150 - 44971 = 8179 labels that don't have the same label for the annotations, predictions, and register.
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as correct_negatives,
	sum(case when annotated_label = 1 then 1 else 0 end) as correct_positives
from beta_product_district.diff_register_predictions_annotations
where annotated_label = predicted_label and annotated_label = register_label;

-- incorrect predictions
-- 3022
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as false_positives,
	sum(case when annotated_label = 1 then 1 else 0 end) as false_negatives
from beta_product_district.diff_register_predictions_annotations
where annotated_label <> predicted_label and annotated_label = register_label;

-- incorrect register labels
-- 4428
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as false_positives,
	sum(case when annotated_label = 1 then 1 else 0 end) as false_negatives
from beta_product_district.diff_register_predictions_annotations
where annotated_label = predicted_label and annotated_label <> register_label;

-- incorrect annotation labels?
-- 729
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as false_positives,
	sum(case when annotated_label = 1 then 1 else 0 end) as false_negatives
from beta_product_district.diff_register_predictions_annotations
where annotated_label <> predicted_label and annotated_label <> register_label;

drop table if exists beta_product_district.tiles_register_wrong;
create table  beta_product_district.tiles_register_wrong;
as 
select 
	*
from beta_product_district.diff_register_predictions_annotations
where annotated_label = predicted_label and annotated_label <> register_label;

drop table if exists beta_product_district.tiles_predictions_wrong;
create table beta_product_district.tiles_predictions_wrong
as 
select 
	*
from beta_product_district.diff_register_predictions_annotations
where annotated_label <> predicted_label and annotated_label = register_label;

drop table if exists beta_product_district.tiles_annotations_maybe_wrong;
create table beta_product_district.tiles_annotations_maybe_wrong
as 
select 
	*
from beta_product_district.diff_register_predictions_annotations
where register_label = predicted_label and annotated_label <> register_label;




--Checks

-- Neighbourhoods
-- 412 districts
select count(distinct(wk_code)) from beta_product_district.selected_districts;

select count(distinct(wk_code)) from public.tiles as ti 
inner join beta_product_district.selected_districts as sn on ST_Intersects(ti.area, sn.wkb_geometry)
inner join beta_product_district.selected_buildings as sb on ST_Intersects(sb.geovlak, ti.area)
where ti.area_id = 19 or ti.area_id = 23
-- 208 districts
select count(*) from beta_product_district.weighted_predictions_aggr_di;

--208
select count(*) from beta_product_district.weighted_diff_per_di_selection;

-- Unique Tiles
-- 116666
select count(*) from beta_product_district.selected_tiles;
-- 116666
select count(distinct(tile_id)) from beta_product_district.selected_tiles;
-- 116666 unique tiles
select count(distinct(tile_id)) from beta_product_district.building_to_tile;
-- 116666
select count(*) from beta_product_district.model_predictions_geo;
-- 116666
select count(distinct(tile_id)) from beta_product_district.model_predictions_geo;
-- 116666
select count(distinct(tile_id)) from beta_product_district.weighted_predictions_per_di;
-- 116666
select count(distinct(tile_id)) from beta_product_district.district_to_tile_building;
-- Unique annotated tiles
-- in public, unfiltered: 62526 --> in beta_product_district, filtered: 53150
select count(*) from beta_product_district.annotations_per_tile_geo;
select count(distinct(tile_id)) from beta_product_district.annotations_per_tile_geo;


-- 121188
select count(*) from beta_product_district.district_to_tile;
-- 121188
select count(*) from beta_product_district.weighted_predictions_per_di;
-- 78824
select count(*) from public.annotations_per_tile;
select count(distinct(UUID)) from public.annotations_per_tile;

--348305
select count(*) from beta_product_district.register_label_per_building;
-- 291929
select count(*) from beta_product_district.building_to_tile;

-- 301039
select count(*) from beta_product_district.district_to_tile_building;
select count(*) from beta_product_district.district_to_tile_building;

-- 54881
select count(*) from beta_product_district.weighted_annotations_per_di;
-- 53150
select count(distinct(tile_id)) from beta_product_district.weighted_annotations_per_di;





-- Old Checks
select count(*) from beta_product_district.selected_districts where in_train_validation_set = true;
select count(distinct(uuid)) from beta_product_district.annotations_per_tile_geo;
-- 116666 tiles, less than we have predictions
select count(distinct(tile_id)) from beta_product_district.district_to_tile_building;
-- 208?
select count(*) from beta_product_district.weighted_register_labels_aggr_di;
select count(*) from beta_product_district.weighted_predictions_aggr_di;
-- 116666 
select count(distinct(tile_id)) from beta_product_district.weighted_predictions_per_di;
-- 208
select count(distinct(wk_code)) from beta_product_district.weighted_predictions_per_di;
-- 116666, same number as selected_tiles, for each of the selected_tiles we have a prediction
select count(*) from beta_product_district.model_predictions_geo;
-- 134379
select count(*) from beta_product_district.model_predictions;

-- 412
select count(*) from beta_product_district.weighted_annotations_aggr_di;
-- 53150 unique tiles in annotation set vs. 116666 for the building set.
-- these 53150 are the tiles that intersect with buildings.
select count(distinct(tile_id)) from beta_product_district.annotations_per_tile_geo;

--count(*) = 347856 = count(distinct(identificatie)) = 347856, all buildings unique.
select count(*) from beta_product_district.selected_buildings;
select count(distinct(identificatie)) from beta_product_district.selected_buildings;

-- Create pv_2017_nl_new table
-- 569667
select count(*) from public.solarpanel_addresses_orig;
-- 544689
select count(*) from beta_product_district.pv_2017_nl_new;
-- 116666
select count(distinct(tile_id)) from beta_product_district.selected_tiles;
-- 116666
select count(distinct(tile_id)) from beta_product_district.district_to_tile;
-- 116666
select count(distinct(tile_id)) from beta_product_district.building_to_tile;
-- 301039 with inner join, gets rid of
select count(*) from beta_product_district.district_to_tile_building;

-- 116666
select count(distinct(tile_id)) from beta_product_district.district_to_tile_building;

-- Important
-- 52 tiles with aggregated label > 1
select sum(num) from
(
    select 1 as num from beta_product_district.building_to_tile
    group by tile_id
    having sum(weighted_register_label_tile) > 1
) tiles_label_bigger_than_one;

select tile_id, sum(weighted_register_label_tile) as num from beta_product_district.building_to_tile
group by tile_id
having sum(weighted_register_label_tile) > 1;

-- still correct?
select
    wk_code, wk_naam,
    sum(tile_area_fraction_in_district) as num_buildings
from beta_product_district.district_to_tile_building
group by wk_code, wk_naam
having sum(tile_area_fraction_in_district) < 30;

select count(*) from beta_product_district.weighted_diff_per_di
where wk_code in
(
    select
        wk_code
    from beta_product_district.district_to_tile_building
    group by wk_code, wk_naam
    having sum(tile_area_fraction_in_district) < 30
);

select wk_naam, perc_diff_predictions_annotations, perc_diff_predictions_register, perc_diff_annotations_register from beta_product_district.weighted_diff_per_di_selection
where wk_naam in ('Gravenrode', 'Brandenberg', 'Klinkerkwartier', 'Gracht';

				  
--Queries venn diagram
--Need to generate:
-- * number of labels agreeing
-- * diff annotation/predictions with register
-- * diff annotations/register with predictions
-- * diff predictions/register with annotations
-- * diff annotation with register?
-- etc.
select 
	count(*) as total,
	sum(case 
		when annotated_label = predicted_label and predicted_label = register_label 
		then 1 else 0 end) as number_of_labels_agreeing,
	sum(case 
		when annotated_label <> predicted_label or predicted_label <> register_label			
		then 1 else 0 end) as number_of_different_labels,
	sum(case 
		when annotated_label = predicted_label and predicted_label <> register_label 
		then 1 else 0 end) as num_diff_from_register,
	sum(case 
		when annotated_label = register_label and register_label <> predicted_label  
		then 1 else 0 end) as num_diff_from_predictions,	
	sum(case 
		when predicted_label = register_label and register_label <> annotated_label  
		then 1 else 0 end) as num_diff_from_annotations,
	sum(case 
		when annotated_label = predicted_label  
		then 1 else 0 end) as num_same_annotations_predictions,
	sum(case 
		when annotated_label = register_label  
		then 1 else 0 end) as num_same_annotations_register,	
	sum(case 
		when predicted_label = register_label 
		then 1 else 0 end) as num_same_predictions_register,	
	sum(case 
		when annotated_label = 1 and annotated_label = predicted_label and predicted_label = register_label 
		then 1 else 0 end) as number_of_positive_labels_agreeing,
	sum(case 
		when annotated_label = 0 and annotated_label = predicted_label and predicted_label = register_label 
		then 1 else 0 end) as number_of_negative_labels_agreeing,
	sum(case 
		when annotated_label = 1 and annotated_label = predicted_label and predicted_label <> register_label 
		then 1 else 0 end) as num_pos_diff_from_register,
	sum(case 
		when annotated_label = 0 and annotated_label = predicted_label and predicted_label <> register_label 
		then 1 else 0 end) as num_neg_diff_from_register,
	sum(case 
		when annotated_label = 1 and annotated_label = register_label and register_label <> predicted_label  
		then 1 else 0 end) as num_pos_diff_from_predictions,	
	sum(case 
		when annotated_label = 0 and annotated_label = register_label and register_label <> predicted_label  
		then 1 else 0 end) as num_neg_diff_from_predictions,
	sum(case 
		when predicted_label = 1 and predicted_label = register_label and register_label <> annotated_label  
		then 1 else 0 end) as num_pos_diff_from_annotations,
	sum(case 
		when predicted_label = 0 and predicted_label = register_label and register_label <> annotated_label  
		then 1 else 0 end) as num_neg_diff_from_annotations
from beta_product_district.diff_register_predictions_annotations
				  
copy
(
	select
		uuid,				  
		case 
			when annotated_label = predicted_label then 1
			else 0
		end as ann_pre,
		case 
			when annotated_label = predicted_label and annotated_label = 1 then 1
			else 0
		end as ann_pre_pos,
		case 
			when annotated_label = register_label then 1
			else 0
		end as ann_reg,
		case 
			when annotated_label = register_label and annotated_label = 1 then 1
			else 0
		end as ann_reg_pos,
		case
			when predicted_label = register_label then 1
			else 0
		end as pre_reg,
		case
			when predicted_label = register_label and predicted_label = 1 then 1
			else 0
		end as pre_reg_pos
	from beta_product_district.diff_register_predictions_annotations
	order by uuid
)
to '/tmp/venn.csv'
csv header delimiter ';';
				  