select *
from weighted_diff_per_nb;

-- Diff # solar panels predictions vs annotations
select 
	bu_code, bu_naam, wk_code, gm_naam, wkb_geometry,
	num_annotations, num_predictions, num_solar_panels_annotations, 
	num_predictions - num_annotations as diff_predictions_annotations,
	num_solar_panels_predictions, predictions_annotations_diff
from weighted_diff_per_nb
where num_annotations > 0;

-- Diff # solar panels register vs annotations
select 
	bu_code, bu_naam, wk_code, gm_naam, wkb_geometry,
	register_annotations_diff
from weighted_diff_per_nb;

-- Diff # solar panels register vs predictions
select 
	bu_code, bu_naam, wk_code, gm_naam, wkb_geometry,
	predictions_annotations_diff
from weighted_diff_per_nb;