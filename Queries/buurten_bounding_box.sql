--Create a table with selected neighbourhoods
drop table selected_neighbourhoods;
create table selected_neighbourhoods
as
select 
	bu.*
from buurt_2017 as bu
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), bu.wkb_geometry);

alter table selected_neighbourhoods
add column in_train_set boolean;

alter table selected_neighbourhoods
add column in_validation_set boolean;

alter table selected_neighbourhoods
add column in_train_validation_set boolean;

update selected_neighbourhoods
set in_train_set = ST_Intersects(wkb_geometry, ai.area)
from areaofinterest as ai
where ai.area_id = 19 and ST_Intersects(wkb_geometry, ai.area);

update selected_neighbourhoods
set in_validation_set = ST_Intersects(wkb_geometry, ai.area)
from areaofinterest as ai
where ai.area_id = 23 and ST_Intersects(wkb_geometry, ai.area);

update selected_neighbourhoods
set in_train_validation_set = in_train_set or in_validation_set;

CREATE INDEX "selected_neighbourhoods_geom_index" ON "selected_neighbourhoods" USING gist (wkb_geometry);

select count(*) from selected_neighbourhoods where in_train_validation_set = true;
-- Create a table with the annotations and the geo information per tile

drop table annotations_per_tile_geo;
create table annotations_per_tile_geo
as
select at.*, ti.tile_id, ti.area as tile_geom, ti.area_id from annotations_per_tile as at
inner join tiles as ti on UUID(ti.uuid) = at.uuid
where label >= 0;

alter table annotations_per_tile_geo 
add constraint pk_annotations_per_tile_geo primary key (id); 

CREATE INDEX "annotations_per_tile_geo_tile_geom_index" ON "annotations_per_tile_geo" USING gist (tile_geom);

select count(distinct(uuid)) from annotations_per_tile_geo;

--Create a table with selected buildings

drop table selected_buildings;
create table selected_buildings
as 
(
	select * from bagactueel.pand as pa 
	where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak) and
	pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
	(pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus)
);

CREATE INDEX "selected_buildings_geom_index" ON "selected_buildings" USING gist (geovlak);


--count(*) = 347856 = count(distinct(identificatie)) = 347856, all buildings unique.
select count(distinct(identificatie)) from selected_buildings;

-- Create pv_2017_nl_new table

-- count(*) pv_2017_nl = 559320, count(*) solarpanel_addresses_orig = 569667, diff= 10347
-- pv_2017_nl: 544689
select count(*) from pv_2017_nl;
select count(*) from solarpanel_addresses_orig;
select count(*) from pv_2017_nl_new;

-- 554100 unique addresses in register when grouped with building_id
-- 554082 unique addresses in register without building_id
-- 18 addresses with double building_id
select sum(num)
from
(
select 1 as num from solarpanel_addresses_orig
group by postcode, number, number_add --, building_id
) a;

-- doubles?
drop table pv_2017_nl_new;
create table pv_2017_nl_new
as
select 
	pv.*,
	af.gid as address_id, af.adresseerbaarobject, af.geopunt as location, pa.gid as pand_id, pa.geovlak as building_polygon 
from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id
inner join bagactueel.pand as pa on pa.identificatie = af.pandid and	
	pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
	pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus;

-- add spatial indexes
-- one building --> multiple addresses
-- one address --> multiple buildings
-- one address --> multiple addresseerobject ids
-- one address --> multiple gids?
-- 660 rows where one address --> multiple buildings
-- 300 rows where one address one building, but multiple occurences, multiple address_ids...
select postcode, number, number_add, address_id, adresseerbaarobject, building_id
from pv_2017_nl_new
order by postcode, number, number_add;

select postcode, huisnummer, huisletter, huisnummertoevoeging, min(adresseerbaarobject), max(adresseerbaarobject), min(gid), max(gid) 
from  bagactueel.adres_full as af 
group by postcode, huisnummer, huisletter, huisnummertoevoeging
having count(*) > 1;

select postcode, number, number_add, address_id, building_id, count(*)
from pv_2017_nl_new
group by postcode, number, number_add, address_id, building_id
--having count(*) > 1
order by postcode, number, number_add, address_id, building_id;

-- number of addresses with addition
select count(*) from bagactueel.adres_full as af
where length(af.huisnummertoevoeging) > 0;


select LPAD(coalesce(af.huisnummertoevoeging, ''), 8, '0') from bagactueel.adres_full as af
limit 100;

-- see if we have doubles in the address register
select 
	af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging, count(*)
from bagactueel.adres_full as af
where (af.pandstatus <> 'Pand gesloopt' and 
	af.pandstatus <> 'Bouwvergunning verleend' and 
	af.pandstatus <> 'Niet gerealiseerd pand') and
	af.verblijfsobjectstatus = 'Verblijfsobject in gebruik'
group by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging
having count(*) > 1
order by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging;

-- number of doubles (one address more buildings) in register
-- 18383 addresses with multiple buildings
select sum(da.double_address) from
(select 
	1 as double_address
from bagactueel.adres_full as af
where (af.pandstatus <> 'Pand gesloopt' and 
	af.pandstatus <> 'Bouwvergunning verleend' and 
	af.pandstatus <> 'Niet gerealiseerd pand') and
	af.verblijfsobjectstatus = 'Verblijfsobject in gebruik'
group by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging
having count(*) > 1) da;

-- double addresses (with more than one building) in ZL: 644 addresses
select sum(da.double_address) from
(select 
	1 as double_address
from bagactueel.adres_full as af
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt) and 
    af.pandstatus <> 'Pand gesloopt' and 
	af.pandstatus <> 'Bouwvergunning verleend' and 
	af.pandstatus <> 'Niet gerealiseerd pand' and
	af.verblijfsobjectstatus = 'Verblijfsobject in gebruik'
group by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging
having count(*) > 1) da;

-- join adres op basis van building_id/vob_id?
select 
	min(af.gid)
from bagactueel.adres_full as af
where (af.pandstatus <> 'Pand gesloopt' and 
	af.pandstatus <> 'Bouwvergunning verleend' and 
	af.pandstatus <> 'Niet gerealiseerd pand') and
	af.verblijfsobjectstatus = 'Verblijfsobject in gebruik'
group by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging, af.nummeraanduiding
order by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging;

-- 569667 solar panel addresses, 559992 addresses with building_id, 9675 without building id
select * from solarpanel_addresses_orig as so
where length(trim(building_id)) = 0

-- 544923 unique address ids
select count(distinct(building_id)) from solarpanel_addresses_orig as so
where length(trim(building_id)) > 0

select postcode, number, number_add, building_id, 
	min(year_in_use) as year_in_use_first, 
	max(year_in_use) as year_in_use_last,
	min(date_in_use) as date_in_use_first,
	max(date_in_use) as date_in_use_last,
	count(*) as number_of_register_entries
from solarpanel_addresses_orig as so
where length(trim(building_id)) > 0
group by postcode, number, number_add, building_id
order by postcode, number, number_add, building_id
limit 100;

-- 544927 addresses
select sum(pv.unique_address) from
(select 1 as unique_address
from solarpanel_addresses_orig as so
where length(trim(building_id)) > 0
group by postcode, number, number_add, building_id
order by postcode, number, number_add, building_id
) pv

-- 544923 addresses
select sum(pv.unique_address) from
(select 1 as unique_address
from solarpanel_addresses_orig as so
where length(trim(building_id)) > 0
group by building_id
order by building_id
) pv

-- Some building ids assigned to multiple postcode, number combinations?
select distinct(sa.building_id), min(pv.postcode), max(pv.postcode), min(pv.number), max(pv.number), count(*)  from solarpanel_addresses_orig sa
inner join
(
	select postcode, number, number_add, building_id
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	order by postcode, number, number_add, building_id
) pv on pv.building_id = sa.building_id
group by sa.building_id
having count(*) > 1;


-- 533994, 543920 (544941-544689=252 without building --> 251 gesloopt of bouwvergunning verleend, 1 geen pandstatus of pandid?)
select * from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id
left join bagactueel.pand as pa on pa.identificatie = af.pandid and
	pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
	pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus
where pa.identificatie is null;
--where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak) and

-- For ZL

-- Number of addresses: 328165
select count(*) from bagactueel.adres_full as af 
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt);

-- Number of addresses with solar panels: 25535 (ZL), 544941 (whole register)
select count(*) from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id	
--where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt); 

-- Number of active addresses with solar panels: 25498, 544689 (whole register) <-- this will be pv_new.
select count(*) from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id
inner join bagactueel.pand as pa on pa.identificatie = af.pandid and	
	pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
	pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus
--where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak); -- pa.identificatie is null and

-- ZL:
-- Difference explained: 37 rows
-- Bouwvergunning verleend: 32
-- Pand in gebruik: 5 ?? --> these are building on the boundary of the ZL bounding box, 
-- because we used pa.geovlak instead of pa.geopunt in the second query these were not taken into account
-- After fixing the query we only get the first category.
-- Whole register:
-- Bouwvergunning verleend: 240
-- Pand gesloopt: 3
-- [null]: 1
-- Where did the Pand in gebruik category above go?

select af1.pandstatus, count(*) from bagactueel.adres_full as af1
inner join (
select building_id from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id	
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt)
except
select building_id from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id
inner join bagactueel.pand as pa on pa.identificatie = af.pandid and	
	pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
	pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt)
) diff on diff.building_id = af1.adresseerbaarobject
group by af1.pandstatus;

-- Other way around no differences, differences from first part of the query above (before except)
select af1.pandstatus, count(*) from bagactueel.adres_full as af1
inner join (
	select building_id from bagactueel.adres_full as af 
	inner join (
		select postcode, number, number_add, building_id, 
			min(year_in_use) as year_in_use_first, 
			max(year_in_use) as year_in_use_last,
			min(date_in_use) as date_in_use_first,
			max(date_in_use) as date_in_use_last,
			count(*) as number_of_register_entries
		from solarpanel_addresses_orig as so
		where length(trim(building_id)) > 0
		group by postcode, number, number_add, building_id
		) pv on af.adresseerbaarobject = pv.building_id
	inner join bagactueel.pand as pa on pa.identificatie = af.pandid and	
		pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
		pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
		pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
		pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus
	where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak)
	except
	select building_id from bagactueel.adres_full as af 
	inner join (
		select postcode, number, number_add, building_id, 
			min(year_in_use) as year_in_use_first, 
			max(year_in_use) as year_in_use_last,
			min(date_in_use) as date_in_use_first,
			max(date_in_use) as date_in_use_last,
			count(*) as number_of_register_entries
		from solarpanel_addresses_orig as so
		where length(trim(building_id)) > 0
		group by postcode, number, number_add, building_id
		) pv on af.adresseerbaarobject = pv.building_id	
	where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt)
) diff on diff.building_id = af1.adresseerbaarobject
group by af1.pandstatus;

select pa1.pandstatus, count(*) from bagactueel.pand as pa1
inner join (
select af.pandid from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id	
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), af.geopunt)
except
select af.pandid from bagactueel.adres_full as af 
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id
inner join bagactueel.pand as pa on pa.identificatie = af.pandid and	
	pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
	pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and 
	pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak)
) diff on diff.pandid = pa1.identificatie
group by pa1.pandstatus;
--group by af.postcode, af.huisnummer, af.huisletter, af.huisnummertoevoeging
--having count(*) > 1
	
select adresseerbaarobject, building_id, pandid  from bagactueel.adres_full as af
inner join (
	select postcode, number, number_add, building_id, 
		min(year_in_use) as year_in_use_first, 
		max(year_in_use) as year_in_use_last,
		min(date_in_use) as date_in_use_first,
		max(date_in_use) as date_in_use_last,
		count(*) as number_of_register_entries
	from solarpanel_addresses_orig as so
	where length(trim(building_id)) > 0
	group by postcode, number, number_add, building_id
	) pv on af.adresseerbaarobject = pv.building_id
where af.postcode = '1018SK' and af.huisnummer = 396
		
	


select distinct(af.verblijfsobjectstatus) from bagactueel.adres_full as af 


select distinct(length(trim(identificatie))) from bagactueel.pand
limit 100;

select distinct(length(trim(building_id)))
from solarpanel_addresses_orig;
	

select * 
from bagactueel.adres_full as af
where postcode = '1011BZ' and huisnummer = '11';

select distinct(pandstatus)
from bagactueel.adres_full as af;

select distinct(verblijfsobjectstatus)
from bagactueel.adres_full as af;
-- number of addresses in solar panel register with addition
select count(*) from solarpanel_addresses_orig as sa
where length(replace(sa.number_add, '00000000', '')) > 0;

select count(*) from
(
	select 1 as num_panels 
	from solarpanel_addresses_orig
	group by postcode, number, number_add
) groups

-- 575264 instead of 569667???
select count(*) from solarpanel_addresses_orig as sa
inner join bagactueel.adres_full as af 
on 
	sa.postcode = af.postcode and 
	length(trim(sa.number)) <> 0 and
	cast(sa.number as numeric) = af.huisnummer and
	sa.number_add = LPAD(coalesce(af.huisnummertoevoeging, ''), 8, '0')
where length(trim(sa.number)) > 0;

-- six addresses without number?
select * from solarpanel_addresses_orig as sa
where length(trim(sa.number)) = 0;

-- Create table with selected pv

drop table selected_pv;
create table selected_pv
as
select

select postcode, number, count(*), min(year_in_use), max(year_in_use), min(date_in_use), max(date_in_use) from pv_2017_nl
group by postcode, number
having count(*) > 1
order by count(*) desc;

-- flat building with multiple subaddresses
select * from pv_2017_nl
where postcode  = '1096BA' and number = '12';

-- same address multiple times?
select * from pv_2017_nl
where postcode  = '5038CB' and number = '31';

-- Create table with selected tiles
drop table selected_tiles;
create table selected_tiles
as
select ti.* from tiles as ti
inner join model_predictions as mp on mp.uuid = UUID(ti.uuid)
where ti.area_id = 19 or ti.area_id = 23;

CREATE INDEX "selected_tiles_geom_index" ON "selected_tiles" USING gist (area);

-- 134379
select count(distinct(tile_id)) from selected_tiles;
-- Assign tiles to neighbourhood
drop table neighbourhood_to_tile;
create table neighbourhood_to_tile
as
select *, 
	ST_Area(ST_Intersection(ti.area, bu.wkb_geometry)) / ST_Area(ti.area) as area_fraction_in_neighbourhood
from selected_neighbourhoods as bu
inner join selected_tiles as ti on ST_Intersects(bu.wkb_geometry, ti.area) 
where in_train_validation_set = true;

select count(distinct(ti.tile_id)) from tiles as ti
inner join tile_files as tf on ti.tile_id = tf.tile_id and tf.layername like '%rgb_hr_2018'
where area_id = 19 or area_id = 23;

-- 134341
select count(distinct(tile_id)) from neighbourhood_to_tile;

-- all missing tiles are part of area 19 = Heerlen
-- 38 tiles in Germany, outside of province border, these belong to buildings who cross the Dutch-German border
create table missing_tiles
as
select * from tiles where tile_id in
(
	select tile_id from selected_tiles
	except
	select tile_id from neighbourhood_to_tile
);


-- Assign buildings to neighbourhood
-- 348305 buildings
drop table register_label_per_building;
create table register_label_per_building
as 
(
	select *, 
		case when pv.pand_id is not null then 1 else 0 end as register_label	
	from selected_buildings as pa
	left join pv_2017_nl_new as pv on pv.pand_id = pa.gid
);

CREATE INDEX "register_label_per_building_geovlak_index" ON "register_label_per_building" USING gist (geovlak);

-- Assign buildings to tile
-- Uses selected_tiles table so also includes the 38 tiles in Germany, 
-- this gets filtered down to 21 because they look for an intersection with a building polygon
drop table building_to_tile;
create table building_to_tile
as
select *,
	ST_Area(ST_Intersection(ti.area, sb.geovlak)) / ST_Area(ti.area) as building_area_fraction_in_tile,
	(ST_Area(ST_Intersection(ti.area, sb.geovlak)) / ST_Area(ti.area)) * register_label as weighted_register_label_tile
from register_label_per_building as sb
inner join selected_tiles as ti on ST_Intersects(sb.geovlak, ti.area);

CREATE INDEX "building_to_tile_geovlak_index" ON "building_to_tile" USING gist (geovlak);
CREATE INDEX "building_to_tile_area_index" ON "building_to_tile" USING gist (area);

-- 116687
select count(distinct(tile_id)) from building_to_tile;

-- 17692 tiles filtered out because the lie outside of the building polygons selected in building_to_tile
create table missing_tiles_bt
as
select * from tiles where tile_id in
(
	select tile_id from selected_tiles
	except
	select tile_id from building_to_tile
);

-- 347856 selected buildings; double buildings
select count(*) from selected_buildings;

-- count(*) = 348964 <> count(distinct(identificatie)) = 347856 --> difference 1108
select count(*) from register_label_per_building;

select count(distinct(identificatie)) from register_label_per_building;

-- find buildings double in register
select 
	identificatie, count(*)
from register_label_per_building
group by identificatie
having count(*) > 1;

-- check if we have same difference when we scrap the multiples
select sum(doubles.number_of_occurences) from
(
	select 
		count(*) - 1 as number_of_occurences
	from register_label_per_building
	group by identificatie
	having count(*) > 1
) doubles;

-- find out which pv addresses have double occurences in the register, multiple occurences in pv register (with multiple dates)
select * from register_label_per_building as rb
inner join pv_2017_nl as pv on pv.pv_id = rb.pv_id
where identificatie in
(
	select 
		identificatie
	from register_label_per_building
	group by identificatie
	having count(*) > 1
)
order by identificatie;

drop table neigbourhood_to_tile_building;
create table neigbourhood_to_tile_building
as
select bt.*, 
	bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_code, bu.gm_naam,
	ST_Area(ST_Intersection(bt.area, bu.wkb_geometry)) / ST_Area(bt.area) as tile_area_fraction_in_neighbourhood,	
	(ST_Area(ST_Intersection(bt.area, bu.wkb_geometry)) / ST_Area(bt.area)) * weighted_register_label_tile as weighted_register_label_tile_nb,
	bu.wkb_geometry
from selected_neighbourhoods as bu
inner join building_to_tile as bt on ST_Intersects(bu.wkb_geometry, bt.area);

CREATE INDEX "neigbourhood_to_tile_building_geovlak_index" ON "neigbourhood_to_tile_building" USING gist (geovlak);
CREATE INDEX "neigbourhood_to_tile_building_area_index" ON "neigbourhood_to_tile_building" USING gist (area);
CREATE INDEX "neigbourhood_to_tile_building_wkb_geometry_index" ON "neigbourhood_to_tile_building" USING gist (wkb_geometry);

-- 301039 with inner join, gets rid of
select count(*) from neigbourhood_to_tile_building;

-- 116666 
select count(distinct(tile_id)) from neigbourhood_to_tile_building;

-- 116687 - 116666 = 21 tiles difference, subset of tiles above that are located in Germany, this is correct.
create table missing_tiles_bt_ntb
as
select * from tiles where tile_id in
(
	select tile_id from building_to_tile
	except
	select tile_id from neigbourhood_to_tile_building
);


select 
	min(tile_area_fraction_in_neighbourhood), max(tile_area_fraction_in_neighbourhood), 
	min(weighted_register_label_tile_nb), max(weighted_register_label_tile_nb) 
from neigbourhood_to_tile_building;

select sum(num) from
(
	select 1 as num from building_to_tile
	group by tile_id
	having sum(weighted_register_label_tile) > 1
) tiles_label_bigger_than_one;

select tile_id, sum(weighted_register_label_tile) as num from building_to_tile
group by tile_id
having sum(weighted_register_label_tile) > 1

select count(*) from weighted_diff_per_nb
where bu_code in
(
	select 
		bu_code 
	from neigbourhood_to_building
	group by bu_code, bu_naam
	having sum(area_fraction_in_neighbourhood) < 30
);


select 
	bu_code, bu_naam, 
	sum(area_fraction_in_neighbourhood) as num_buildings
from neigbourhood_to_building
group by bu_code, bu_naam
having sum(area_fraction_in_neighbourhood) < 30;
	
	
-- Create weighing table for annotations
drop table weighted_annotations_per_nb;
create table weighted_annotations_per_nb
as 
select 
	ag.*,  
	nt.bu_code, nt.bu_naam, nt.wk_code, nt.gm_code, nt.gm_naam, nt.area_fraction_in_neighbourhood,
	ag.label * nt.area_fraction_in_neighbourhood as weighted_label 
from annotations_per_tile_geo as ag
inner join neighbourhood_to_tile as nt on nt.tile_id = ag.tile_id;

-- 62526 unique tiles in annotation set vs. 116666 for the building set.
select count(distinct(tile_id)) from annotations_per_tile_geo;

alter table weighted_annotations_per_nb
add constraint pk_weighted_annotations_per_nb
primary key (bu_code, tile_id);

select count(*) from weighted_annotations_per_nb
order by tile_id;

-- Number of annotations/annotated solar panels per neighbourhood in Zuid Limburg
drop table weighted_annotations_aggr_nb;
create table weighted_annotations_aggr_nb
as
select 
	bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, 
	count(distinct(id)) as num_annotations, 	
	sum(coalesce(area_fraction_in_neighbourhood, 0)) as number_of_tiles_annotated,
	sum(
		case when 
			weighted_label < 0 
		then 0 
		else coalesce(weighted_label, 0) 
		end
	) as num_solar_panels_annotations, 
	wkb_geometry 
from selected_neighbourhoods as bu
left join weighted_annotations_per_nb as wa on bu.bu_code = wa.bu_code
group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, wkb_geometry;

select count(*) from weighted_annotations_aggr_nb;

-- Create weighing table for model predictions
drop table model_predictions;
create table model_predictions
(		
	uuid_string varchar(36),	
	prediction double precision,
	label int
);

copy model_predictions (uuid_string, prediction, label)
from '/media/tdjg/Data1/DeepSolaris/all_predictions.csv'
csv delimiter ';' header;

alter table model_predictions
add column prediction_id serial;

alter table model_predictions
add column uuid UUID;

alter table model_predictions
add column model_name varchar;

delete from model_predictions
where uuid_string like 'feh%';

update model_predictions
set uuid = UUID(uuid_string);

update model_predictions
set model_name = 'vgg16_best';


alter table model_predictions
drop column uuid_string;

-- delete double uuids...
select count(*)
from model_predictions
where uuid in
(
	select uuid from model_predictions
	group by uuid
	having count(uuid) > 1
);

select count(*) from model_predictions;

delete from model_predictions 
where prediction_id in
(
	select max(prediction_id) from model_predictions	
	group by uuid
	having count(uuid) > 1
);

alter table model_predictions
add constraint pk_model_predictions primary key (uuid, model_name);

drop table model_predictions_geo;
create table model_predictions_geo
as
select mp.*, ti.tile_id, ti.area_id, ti.area as tile_geom
from model_predictions as mp
inner join selected_tiles as ti on UUID(ti.uuid) = mp.uuid;

-- 134379, same number as selected_tiles, for each of the selected_tiles we have a prediction
select count(*) from model_predictions_geo;

alter table model_predictions_geo
add constraint pk_model_predictions_geo 
primary key  (tile_id);

create index model_predictions_geo_idx
on model_predictions_geo
using gist(tile_geom);

select distinct(area_id) 
from model_predictions_geo;

drop table weighted_predictions_per_nb;
create table weighted_predictions_per_nb;
as 
select 
	mp.*,  
	nt.bu_code, nt.bu_naam, nt.wk_code, nt.gm_code, nt.gm_naam, nt.area_fraction_in_neighbourhood,
	mp.label * nt.area_fraction_in_neighbourhood as weighted_label 
from model_predictions_geo as mp
inner join neighbourhood_to_tile as nt on nt.tile_id = mp.tile_id;

-- 134341 again without the 38 tiles located in Germany
select count(distinct(tile_id)) from weighted_predictions_per_nb;

select count(distinct(bu_code)) from weighted_predictions_per_nb;

alter table weighted_predictions_per_nb
add constraint pk_weighted_predictions_per_nb
primary key (bu_code, tile_id);

select * from weighted_predictions_per_nb
order by tile_id;

-- Model predictions per neighbourhood
select 
	mp.area_id,
	sum(case when at.label = 1 and at.label = mp.label then 1 else 0 end) as positives,
	sum(case when at.label = 0 and mp.label = 1 then 1 else 0 end) as false_positives,
	sum(case when at.label = 0 and at.label = mp.label then 1 else 0 end) as negatives,
	sum(case when at.label = 1 and mp.label = 0 then 1 else 0 end) as false_negatives,
	sum(case when at.label = 1 then 1 else 0 end) as num_positives,
	sum(case when at.label = 0 then 1 else 0 end) as num_negatives,
	count(*) as total
from annotations_per_tile_geo as at
inner join model_predictions_geo as mp on at.uuid = UUID(mp.uuid)
where at.label <> -1
group by mp.area_id
order by mp.area_id;

drop table weighted_predictions_aggr_nb;
create table weighted_predictions_aggr_nb
as
select
	bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, 
	sum(case when wp.weighted_label is not null then 1 else 0 end) as num_predictions,
	sum(coalesce(area_fraction_in_neighbourhood, 0)) as weighted_num_predictions,
	sum(
		case when 
			weighted_label < 0 
		then 0 
		else coalesce(weighted_label, 0) 
		end
	) as num_solar_panels_predictions, 
	wkb_geometry 
from selected_neighbourhoods as bu
left join weighted_predictions_per_nb as wp on bu.bu_code = wp.bu_code
group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, wkb_geometry;

select count(*) from weighted_predictions_aggr_nb;

-- Create weighing table for register labels per neighbourhood

--drop table weighted_register_labels_per_nb;
--create table weighted_register_labels_per_nb
--as 
--select 
--	*,
--	register_label * area_fraction_in_neighbourhood as weighted_label 
--from neigbourhood_to_tile_building;

select count(distinct(bu_code)) from neigbourhood_to_building;

select building_id, area_fraction_in_neighbourhood, weighted_label 
from weighted_register_labels_per_nb
order by building_id;

select count(*)
from bagactueel.pandactueel as pa
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak);

drop table weighted_register_labels_aggr_nb;
create table weighted_register_labels_aggr_nb
as
select 
	bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, 
	count(*) as num_register_labels, 
	sum(coalesce(tile_area_fraction_in_neighbourhood, 0)) as weighted_tiles_neighbourhood,
	sum(coalesce(weighted_register_label_tile_nb, 0)) as num_solar_panels_register, 
	wkb_geometry 
from neigbourhood_to_tile_building as bu
group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, bu.wkb_geometry;

-- 116666 tiles, less than we have predictions
select count(distinct(tile_id)) from neigbourhood_to_tile_building;
select count(*) from weighted_register_labels_aggr_nb;

-- Create diff tables per neighbourhood
-- We need differences between labels!! Not just in numbers.
-- should num_predictions, num_annotations, num_register_labels be weighted?
drop table weighted_diff_per_nb;
create table weighted_diff_per_nb
as
select wpa.bu_code, wpa.bu_naam, wpa.wk_code, wpa.gm_naam, 
		num_annotations,
		num_annotations / ST_Area(wpa.wkb_geometry) as num_annotations_norm,
		num_solar_panels_annotations,
		num_solar_panels_annotations / ST_Area(wpa.wkb_geometry) as num_solar_panels_annotations_norm, 
		case when num_annotations > 0 then 
			num_solar_panels_annotations / number_of_tiles_annotated
			else 0 
		end as perc_solar_panel_annotations_of_annotations,  
		case when num_annotations > 0 then 
			num_solar_panels_annotations / num_annotations
			else 0 
		end as perc_solar_panel_annotations_of_tiles, 
		num_predictions,
		num_predictions / ST_Area(wpa.wkb_geometry) as num_predictions_norm,
		num_solar_panels_predictions,
		num_solar_panels_predictions / ST_Area(wpa.wkb_geometry) as num_solar_panels_predictions_norm, 
		case when weighted_num_predictions > 0 then 
			num_solar_panels_predictions / weighted_num_predictions 
			else 0 
		end as perc_solar_panel_predictions,
		num_register_labels,
		num_register_labels / ST_Area(wpa.wkb_geometry) as num_register_labels_norm,
		num_solar_panels_register,	
		case when weighted_tiles_neighbourhood > 0 then
			num_solar_panels_register / weighted_tiles_neighbourhood 
			else 0 
		end as perc_solar_panels_register,
		num_solar_panels_register / ST_Area(wpa.wkb_geometry) as num_solar_panels_register_norm,
		num_solar_panels_predictions - num_solar_panels_annotations as predictions_annotations_diff,
		(num_solar_panels_predictions - num_solar_panels_annotations) / ST_Area(wpa.wkb_geometry) as predictions_annotations_diff_norm,
		num_solar_panels_register - num_solar_panels_annotations as register_annotations_diff,
		(num_solar_panels_register - num_solar_panels_annotations) / ST_Area(wpa.wkb_geometry) as register_annotations_diff_norm,
		num_solar_panels_register - num_solar_panels_predictions as register_predictions_diff,
		(num_solar_panels_register - num_solar_panels_predictions) / ST_Area(wpa.wkb_geometry) as register_predictions_diff_norm,
		wpa.wkb_geometry		
from weighted_predictions_aggr_nb as wpa
inner join weighted_annotations_aggr_nb as waa on waa.bu_code = wpa.bu_code
inner join weighted_register_labels_aggr_nb as wra on wra.bu_code = wpa.bu_code

drop table weighted_diff_per_nb_selection;
create table weighted_diff_per_nb_selection
as
select 
	bu_code, bu_naam, wk_code, gm_naam, 
	perc_solar_panel_annotations_of_tiles,
	min(perc_solar_panel_annotations_of_tiles) over() as min_perc_solar_panel_annotations_of_tiles,
	max(perc_solar_panel_annotations_of_tiles) over() as max_perc_solar_panel_annotations_of_tiles,
	perc_solar_panel_predictions,
	min(perc_solar_panel_predictions) over() as min_perc_solar_panel_predictions,
	max(perc_solar_panel_predictions) over() as max_perc_solar_panel_predictions,
	perc_solar_panels_register,	
	min(perc_solar_panels_register) over() as min_perc_solar_panel_register,
	max(perc_solar_panels_register) over() as max_perc_solar_panel_register,
	perc_solar_panel_predictions - perc_solar_panel_annotations_of_tiles as perc_diff_predictions_annotations,
	min(perc_solar_panel_predictions - perc_solar_panel_annotations_of_tiles) over() as min_perc_diff_predictions_annotations,
	max(perc_solar_panel_predictions - perc_solar_panel_annotations_of_tiles) over() as max_perc_diff_predictions_annotations,
	perc_solar_panel_predictions - perc_solar_panels_register as perc_diff_predictions_register,
	min(perc_solar_panel_predictions - perc_solar_panels_register) over() as min_perc_diff_predictions_register,
	max(perc_solar_panel_predictions - perc_solar_panels_register) over() as max_perc_diff_predictions_register,
	perc_solar_panel_annotations_of_tiles - perc_solar_panels_register as perc_diff_annotations_register,
	min(perc_solar_panel_annotations_of_tiles - perc_solar_panels_register) over() as min_perc_diff_annotations_register,
	max(perc_solar_panel_annotations_of_tiles - perc_solar_panels_register) over() as max_perc_diff_annotations_register,
	wkb_geometry
from weighted_diff_per_nb
where num_predictions > 0;

select num_predictions 
from weighted_diff_per_nb

CREATE INDEX "weighted_diff_per_nb_selection_wkb_geometry_index" ON "weighted_diff_per_nb_selection" USING gist (wkb_geometry);


select count(*) from weighted_diff_per_nb;		

select  
	bu_code, bu_naam, wk_code, gm_naam,
	num_annotations,
	perc_solar_panel_annotations_of_annotations,
	perc_solar_panel_annotations_of_tiles,
	num_predictions,
	perc_solar_panel_predictions,
	num_register_labels,
	perc_solar_panels_register, 
	wkb_geometry
from weighted_diff_per_nb
where num_annotations > 0;

-- diff between percentages
drop table perc_solar_panel_differences_per_nb;
create table perc_solar_panel_differences_per_nb
as
select  
	bu_code, bu_naam, wk_code, gm_naam,
	--perc_solar_panel_annotations_of_annotations,
	perc_solar_panel_annotations_of_tiles,
	perc_solar_panel_predictions,
	perc_solar_panels_register, 
	perc_solar_panels_register - perc_solar_panel_annotations_of_tiles as perc_diff_register_annotations,
	perc_solar_panel_predictions - perc_solar_panel_annotations_of_tiles as perc_diff_predictions_annotations,
	perc_solar_panels_register - perc_solar_panel_predictions as perc_diff_register_predictions,
	wkb_geometry
from weighted_diff_per_nb
where num_annotations > 0;


select * from weighted_diff_per_nb
limit 100;
-- Number of annotations BB Heerlen
select count(*) from annotations_per_tile_geo
where ST_Contains(ST_MakeEnvelope(190700, 327600, 200000, 314500, 28992), tile_geom);

select count(*) from annotations_per_tile_geo
where ST_Intersects(ST_MakeEnvelope(190700, 327600, 200000, 314500, 28992), tile_geom);

-- Number of annotations ZL_HR
select count(*) from annotations_per_tile_geo
where ST_Contains(ST_MakeEnvelope(181300, 327600, 190600, 314500, 28992), tile_geom);

select count(*) from annotations_per_tile_geo
where ST_Intersects(ST_MakeEnvelope(181300, 327600, 190600, 314500, 28992), tile_geom);

-- Number of model predictions
select count(*) from model_predictions;

-- BB ZL_HR
select * from buurt_2017
where ST_Contains(ST_MakeEnvelope(181300, 327600, 190600, 314500, 28992), wkb_geometry);
-- BB Heerlen
select * from buurt_2017
where ST_Contains(ST_MakeEnvelope(190700, 327600, 200000, 314500, 28992), wkb_geometry);
--Whole ZL
select * from buurt_2017 
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), wkb_geometry)
and bu_code = 'BU08990337';

create index pv_2017_nl_geom_idx
on pv_2017_nl
using GIST (location);
--Solar panels in ZL
select * from pv_2017_nl
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), location);

-- Solar panels in ZL BB
select count(*) from pv_2017_nl
where ST_Contains(ST_MakeEnvelope(181300, 327600, 190600, 314500, 28992), location);
-- Solar panels in Heerlen BB
select count(*) from pv_2017_nl
where ST_Contains(ST_MakeEnvelope(190700, 327600, 200000, 314500, 28992), location);

-- Number of solar panels per neighbourhood in Zuid Limburg
select bu_code, bu_naam, wk_code, gm_naam, count(pv_id) as num_solar_panels, wkb_geometry from buurt_2017 as bu
left join pv_2017_nl as pv on ST_Contains(wkb_geometry, location)
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), wkb_geometry)
group by bu_code, bu_naam, wk_code, gm_naam, wkb_geometry
having count(pv_id) = 0;

-- Unique register entries
select count(distinct(bag_address_id)) from buurt_2017 as bu
inner join pv_2017_nl as pv on ST_Contains(wkb_geometry, location)
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), wkb_geometry);




--neighbourhood_to_tile
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), wkb_geometry)
group by bu_code, bu_naam, wk_code, gm_naam, wkb_geometry;

-- Combined: number of solar panels from register, number of annotations/annotated solar panels, difference between register and annotations per buurt
drop table num_solar_panels_bu_ann_vs_reg;
create table num_solar_panels_bu_ann_vs_reg
as
(
	select 
		bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, 
		min(spb.num_solar_panels) as num_solar_panels_register,
		count(distinct(an.id)) as num_annotations, 
		sum(
			case when 
				an.label = -1 
			then 0 
			else coalesce(an.label, 0) 
			end
		) as num_solar_panels_annotations, 
		bu.wkb_geometry 
	from buurt_2017 as bu
	inner join annotations_per_tile_geo as an on ST_Contains(bu.wkb_geometry, an.tile_geom)
	inner join
	(
		select bu_code, bu_naam, wk_code, gm_naam, count(pv_id) as num_solar_panels, wkb_geometry from buurt_2017 as bu
		left join pv_2017_nl as pv on ST_Contains(wkb_geometry, location)
		where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), wkb_geometry)
		group by bu_code, bu_naam, wk_code, gm_naam, wkb_geometry
	) spb on spb.bu_code = bu.bu_code
	where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), bu.wkb_geometry) 
	group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, bu.wkb_geometry
);

-- Compare number of solar panels/annotations with original tables --> find missing values
select 
	sum(num_solar_panels_register) as total_solar_panels_register,
	sum(num_solar_panels_annotations) as total_solar_panels_annotations
from num_solar_panels_bu_ann_vs_reg;

select sum(label) from annotations_per_tile_geo;

select count(*) from pv_2017_nl
where ST_Contains(ST_MakeEnvelope(181300, 327600, 190600, 314500, 28992), location);
-- Solar panels in Heerlen BB
select count(*) from pv_2017_nl
where ST_Contains(ST_MakeEnvelope(190700, 327600, 200000, 314500, 28992), location);


 

