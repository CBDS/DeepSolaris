--Create a separate schema for the beta product to keep the other database clean
create schema if not exists beta_product;

-- drop beta product tables in public schema
drop table if exists public.annotations_per_tile_geo;
drop table if exists public.building_to_tile;
drop table if exists public.model_predictions;
drop table if exists public.model_predictions_geo;
drop table if exists public.neighbourhood_to_tile;
drop table if exists public.neigbourhood_to_tile_building;
drop table if exists public.perc_solar_panel_differences_per_nb;
drop table if exists public.pv_2017_nl_new;
drop table if exists public.register_label_per_building;
drop table if exists public.selected_buildings;
drop table if exists public.selected_neighbourhoods;
drop table if exists public.selected_tiles;
drop table if exists public.weighted_annotations_aggr_nb;
drop table if exists public.weighted_annotations_per_nb;
drop table if exists public.weighted_diff_per_nb;
drop table if exists public.weighted_diff_per_nb_selection;
drop table if exists public.weighted_predictions_aggr_nb;
drop table if exists public.weighted_predictions_per_nb;
drop table if exists public.weighted_register_labels_aggr_nb;

--Create a table with selected neighbourhoods
drop table if exists beta_product.selected_neighbourhoods;
create table beta_product.selected_neighbourhoods
as
select
    bu.*
from public.buurt_2017 as bu
where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), bu.wkb_geometry);

alter table beta_product.selected_neighbourhoods
add column in_train_set boolean;

alter table beta_product.selected_neighbourhoods
add column in_validation_set boolean;

alter table beta_product.selected_neighbourhoods
add column in_train_validation_set boolean;

update beta_product.selected_neighbourhoods
set in_train_set = ST_Intersects(wkb_geometry, ai.area)
from public.areaofinterest as ai
where ai.area_id = 19 and ST_Intersects(wkb_geometry, ai.area);

update beta_product.selected_neighbourhoods
set in_validation_set = ST_Intersects(wkb_geometry, ai.area)
from public.areaofinterest as ai
where ai.area_id = 23 and ST_Intersects(wkb_geometry, ai.area);

update beta_product.selected_neighbourhoods
set in_train_validation_set = in_train_set or in_validation_set;

CREATE INDEX "selected_neighbourhoods_geom_index" ON beta_product.selected_neighbourhoods USING gist (wkb_geometry);

--Create a table beta_product.with selected buildings
drop table if exists beta_product.selected_buildings;
create table beta_product.selected_buildings
as
(
    select * from bagactueel.pand as pa
    where ST_Contains(ST_MakeEnvelope(172700, 306800, 205000,  338400, 28992), pa.geovlak) and
    pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
    (pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and
    pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and
    pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus)
);

CREATE INDEX "selected_buildings_geom_index" ON beta_product.selected_buildings USING gist (geovlak);

-- Create table with selected tiles
-- changed this to only include tiles that intersect with buildings
-- create table containing the annotated tiles that are contain buildings and the tiles that are inside a neighbourhood;
-- some tiles were located in Germany.
drop table if exists beta_product.selected_tiles;
create table beta_product.selected_tiles
as
select ti.* from public.tiles as ti
inner join (
	select distinct(tile_id) from public.tiles as ti 
	inner join beta_product.selected_buildings as sb on ST_Intersects(sb.geovlak, ti.area)
	inner join beta_product.selected_neighbourhoods as sn on ST_Intersects(ti.area, sn.wkb_geometry)
	where ti.area_id = 19 or ti.area_id = 23
) st on st.tile_id = ti.tile_id;

CREATE INDEX "selected_tiles_geom_index" ON beta_product.selected_tiles USING gist (area);

-- Assign tiles to neighbourhood
-- 121188 items?
drop table if exists beta_product.neighbourhood_to_tile;
create table beta_product.neighbourhood_to_tile
as
select *,
    ST_Area(ST_Intersection(ti.area, bu.wkb_geometry)) / ST_Area(ti.area) as area_fraction_in_neighbourhood
from beta_product.selected_neighbourhoods as bu
inner join beta_product.selected_tiles as ti on ST_Intersects(bu.wkb_geometry, ti.area)
where in_train_validation_set = true;

CREATE INDEX "neighbourhood_to_tile_area_index" ON beta_product.neighbourhood_to_tile USING gist (area);
CREATE INDEX "neighbourhood_to_tile_wkb_geometry_index" ON beta_product.neighbourhood_to_tile USING gist (wkb_geometry);

-- Create a table beta_product.with the annotations and the geo information per tile
-- Only use the tiles from table selected_tiles that are in the area of interest.
-- Only use annotations that have a label 0 (= does not contain solar panels) or 1 (=contains solar panels)
drop table if exists beta_product.annotations_per_tile_geo;
create table beta_product.annotations_per_tile_geo
as
select at.*, ti.tile_id, ti.area as tile_geom, ti.area_id from public.annotations_per_tile as at
inner join beta_product.selected_tiles as ti on UUID(ti.uuid) = at.uuid  
where label >= 0;

alter table beta_product.annotations_per_tile_geo
add constraint pk_annotations_per_tile_geo primary key (id);

CREATE INDEX "annotations_per_tile_geo_tile_geom_index" ON beta_product.annotations_per_tile_geo USING gist (tile_geom);

-- doubles?
-- 544689
drop table if exists beta_product.pv_2017_nl_new;
create table beta_product.pv_2017_nl_new
as
select
    pv.*,
    af.gid as address_id, af.adresseerbaarobject, af.geopunt as location, pa.gid as pand_id, pa.geovlak as building_polygon
from bagactueel.adres_full as af
inner join (
    select postcode, number, number_add, building_id,
        min(year_in_use) as year_in_use_first,
        max(year_in_use) as year_in_use_last,
        min(date_in_use) as date_in_use_first,
        max(date_in_use) as date_in_use_last,
        count(*) as number_of_register_entries
    from public.solarpanel_addresses_orig as so
    where length(trim(building_id)) > 0
    group by postcode, number, number_add, building_id
    ) pv on af.adresseerbaarobject = pv.building_id
inner join bagactueel.pand as pa on pa.identificatie = af.pandid and   
    pa.aanduidingrecordinactief = false and pa.einddatumtijdvakgeldigheid is null and
    pa.pandstatus <> 'Pand gesloopt'::bagactueel.pandstatus and
    pa.pandstatus <> 'Bouwvergunning verleend'::bagactueel.pandstatus and
    pa.pandstatus <> 'Niet gerealiseerd pand'::bagactueel.pandstatus;

-- Assign buildings to neighbourhood
-- 348305 buildings
drop table if exists beta_product.register_label_per_building;
create table beta_product.register_label_per_building
as
(
    select *,
        case when pv.pand_id is not null then 1 else 0 end as register_label   
    from beta_product.selected_buildings as pa
    left join beta_product.pv_2017_nl_new as pv on pv.pand_id = pa.gid
);

CREATE INDEX "register_label_per_building_geovlak_index" ON beta_product.register_label_per_building USING gist (geovlak);

-- Assign buildings to tile
-- Uses selected_tiles table beta_product.so also includes the 38 tiles in Germany,
-- this gets filtered down to 21 because they look for an intersection with a building polygon
drop table if exists beta_product.building_to_tile;
create table beta_product.building_to_tile
as
select *,
    ST_Area(ST_Intersection(ti.area, sb.geovlak)) / ST_Area(ti.area) as building_area_fraction_in_tile    
from beta_product.register_label_per_building as sb
inner join beta_product.selected_tiles as ti on ST_Intersects(sb.geovlak, ti.area);

CREATE INDEX "building_to_tile_geovlak_index" ON beta_product.building_to_tile USING gist (geovlak);
CREATE INDEX "building_to_tile_area_index" ON beta_product.building_to_tile USING gist (area);

drop table if exists beta_product.neighbourhood_to_tile_building;
create table beta_product.neighbourhood_to_tile_building
as
select bt.*,
    bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_code, bu.gm_naam,
	ST_Area(ST_Intersection(bt.area, bu.wkb_geometry)) / ST_Area(bt.area) as tile_area_fraction_in_neighbourhood,
	bu.wkb_geometry
from beta_product.selected_neighbourhoods as bu
inner join beta_product.building_to_tile as bt on ST_Intersects(bu.wkb_geometry, bt.area);

CREATE INDEX "neigbourhood_to_tile_building_geovlak_index" ON beta_product.neighbourhood_to_tile_building USING gist (geovlak);
CREATE INDEX "neigbourhood_to_tile_building_area_index" ON beta_product.neighbourhood_to_tile_building USING gist (area);
CREATE INDEX "neigbourhood_to_tile_building_wkb_geometry_index" ON beta_product.neighbourhood_to_tile_building USING gist (wkb_geometry);

-- Create weighing table beta_product.for annotations
drop table if exists beta_product.weighted_annotations_per_nb;
create table beta_product.weighted_annotations_per_nb
as
select
    ag.*, 
    nt.bu_code, nt.bu_naam, nt.wk_code, nt.gm_code, nt.gm_naam, nt.area_fraction_in_neighbourhood    
from beta_product.annotations_per_tile_geo as ag
inner join beta_product.neighbourhood_to_tile as nt on nt.tile_id = ag.tile_id;

alter table beta_product.weighted_annotations_per_nb
add constraint pk_weighted_annotations_per_nb
primary key (bu_code, tile_id);

-- Number of annotations/annotated solar panels per neighbourhood in Zuid Limburg
drop table if exists beta_product.weighted_annotations_aggr_nb;
create table beta_product.weighted_annotations_aggr_nb
as
select
    bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam,
    count(distinct(id)) as num_annotations,    
    sum(coalesce(area_fraction_in_neighbourhood, 0)) as number_of_tiles_annotated,
    sum(
        case 
			when wa.label != 1 then 0
			when wa.label = 1 then 1 * wa.area_fraction_in_neighbourhood		
        end
    )  as num_solar_panels_annotations,
	sum(
        case 
			when wa.label != 0 then 0
			when wa.label = 0 then 1 * wa.area_fraction_in_neighbourhood		
        end
    ) as num_negative_annotations,
    wkb_geometry
from beta_product.selected_neighbourhoods as bu
left join beta_product.weighted_annotations_per_nb as wa on bu.bu_code = wa.bu_code
group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, wkb_geometry;

-- Create weighing table beta_product.for model predictions
drop table if exists beta_product.model_predictions;
create table beta_product.model_predictions
(      
    uuid_string varchar(36),   
    prediction double precision,
    label int
);

-- import does not work for some reason
-- import with import wizard pgadmin 4
-- copy beta_product.model_predictions (uuid_string, prediction, label)
-- from '/media/tdjg/Data1/DeepSolaris/all_predictions.csv'
-- csv delimiter ';' header;

alter table beta_product.model_predictions
add column prediction_id serial;

alter table beta_product.model_predictions
add column uuid UUID;

alter table beta_product.model_predictions
add column model_name varchar;

delete from beta_product.model_predictions
where uuid_string like 'feh%';

update beta_product.model_predictions
set uuid = UUID(uuid_string);

update beta_product.model_predictions
set model_name = 'vgg16_best';

alter table beta_product.model_predictions
drop column uuid_string;

delete from beta_product.model_predictions
where prediction_id in
(
    select max(prediction_id) from beta_product.model_predictions
    group by uuid
    having count(uuid) > 1
);

alter table beta_product.model_predictions
add constraint pk_model_predictions primary key (uuid, model_name);

drop table if exists beta_product.model_predictions_geo;
create table beta_product.model_predictions_geo
as
select mp.*, ti.tile_id, ti.area_id, ti.area as tile_geom
from beta_product.model_predictions as mp
inner join beta_product.selected_tiles as ti on UUID(ti.uuid) = mp.uuid;

alter table beta_product.model_predictions_geo
add constraint pk_model_predictions_geo
primary key  (tile_id);

create index model_predictions_geo_idx
on beta_product.model_predictions_geo
using gist(tile_geom);

drop table if exists beta_product.weighted_predictions_per_nb;
create table beta_product.weighted_predictions_per_nb
as
select
    mp.*, 
    nt.bu_code, nt.bu_naam, nt.wk_code, nt.gm_code, nt.gm_naam, nt.area_fraction_in_neighbourhood    
from beta_product.model_predictions_geo as mp
inner join beta_product.neighbourhood_to_tile as nt on nt.tile_id = mp.tile_id;

alter table beta_product.weighted_predictions_per_nb
add constraint pk_weighted_predictions_per_nb
primary key (bu_code, tile_id);

-- Model predictions per neighbourhood
drop table if exists beta_product.weighted_predictions_aggr_nb;
create table beta_product.weighted_predictions_aggr_nb
as
select
    bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam,
    count(distinct(prediction_id)) as num_predictions,
    sum(coalesce(area_fraction_in_neighbourhood, 0)) as weighted_num_predictions,
    sum(
        case 
			when wp.label != 1 then 0
			when wp.label = 1 then 1 * wp.area_fraction_in_neighbourhood		
        end
    )  as num_solar_panels_predictions,
	sum(
        case 
			when wp.label != 0 then 0
			when wp.label = 0 then 1 * wp.area_fraction_in_neighbourhood		
        end
    ) as num_negative_predictions,
    wkb_geometry
from beta_product.selected_neighbourhoods as bu
inner join beta_product.weighted_predictions_per_nb as wp on bu.bu_code = wp.bu_code
group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, wkb_geometry;

-- Create weighing table beta_product.for register labels per neighbourhood
-- 208 neighbourhoods

drop table if exists beta_product.weighted_register_labels_aggr_nb;
create table beta_product.weighted_register_labels_aggr_nb
as
select
    bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam,
    count(*) as num_register_labels,
	sum(coalesce(tile_area_fraction_in_neighbourhood, 0)) as weighted_tiles_neighbourhood,
   	sum(coalesce(tile_area_fraction_in_neighbourhood * building_area_fraction_in_tile, 0)) as weighted_number_register_labels,
    	sum(
			case 
				when register_label != 1 then 0
				when register_label = 1 then 1 * tile_area_fraction_in_neighbourhood * building_area_fraction_in_tile		
			end
		)  as num_solar_panels_register,
		sum(
			case 
				when register_label != 0 then 0
				when register_label = 0 then 1 * tile_area_fraction_in_neighbourhood * building_area_fraction_in_tile	
			end
		) as num_negative_register_labels,
     wkb_geometry
from beta_product.neighbourhood_to_tile_building as bu
group by bu.bu_code, bu.bu_naam, bu.wk_code, bu.gm_naam, bu.wkb_geometry;

CREATE INDEX "weighted_register_labels_aggr_nb_wkb_geometry_index" ON beta_product.weighted_register_labels_aggr_nb USING gist (wkb_geometry);


-- Create diff tables per neighbourhood
-- We need differences between labels!! Not just in numbers.
-- should num_predictions, num_annotations, num_register_labels be weighted?
-- 208 neighbourhoods.

-- Diff predictions - annotations on annotated tiles
drop table if exists beta_product.diff_predictions_annotations;
create table beta_product.diff_predictions_annotations
as
select
	atg.tile_id, atg.tile_geom, 
	atg.total_annotations, atg.num_positives, atg.num_negatives, atg.default_annotation,
	atg.area_id, atg.uuid, mp.model_name,
	atg.label as annotated_label, 
	mp.label as predicted_label
from beta_product.annotations_per_tile_geo as atg
inner join beta_product.model_predictions_geo as mp on mp.tile_id = atg.tile_id;

select 
	sum(case when annotated_label = 1 and predicted_label = 0 then 1 else 0 end) as false_negatives,
	sum(case when annotated_label = 0 and predicted_label = 1 then 1 else 0 end) as false_positives
from beta_product.diff_predictions_annotations
where predicted_label <> annotated_label;

-- Diff register - annotations on annotated tiles
-- Here we assign a register label based on the weighted label
-- As register labels are weighted by the area of the building, we assume that at least three panels
-- need to be visible to be able to be counted as label 1. Each solar panel is 1x1.5m approximately or
-- 10x15pixels = 150 pixels in total, 3 solar panels is 450 pixels, is 0.01 (1%) of the total area of the tile.
drop table if exists beta_product.building_to_tile_aggr;
create table beta_product.building_to_tile_aggr
as
select 
	tile_id, 
	sum(coalesce(building_area_fraction_in_tile, 0)) as weighted_number_register_labels_tile,
	sum(
		case 
			when register_label != 1 then 0
			when register_label = 1 then 1 * building_area_fraction_in_tile		
		end
	)  as num_solar_panels_register_tile,
	sum(
		case 
			when register_label != 0 then 0
			when register_label = 0 then 1 * building_area_fraction_in_tile	
		end
	) as num_negative_register_labels_tile,	
	case 
		when sum(register_label * building_area_fraction_in_tile) < 0.01 then 0
		else 1
	end as register_label,
	count(*) as num_buildings,
	bt.area
from beta_product.building_to_tile as bt
group by bt.tile_id, bt.area;

-- 116666
select count(distinct(tile_id)) from beta_product.building_to_tile_aggr;

select 
	min(register_label), max(register_label) 
from building_to_tile_aggr;


drop table if exists beta_product.diff_register_annotations;
create table beta_product.diff_register_annotations
as
select
	atg.tile_id, atg.tile_geom, 
	atg.total_annotations, atg.num_positives, atg.num_negatives, atg.default_annotation,
	atg.area_id, atg.uuid, 
	atg.label as annotated_label, 
	rp.register_label as register_label, 
	rp.weighted_number_register_labels_tile,
	rp.num_solar_panels_register_tile, 
	rp.num_negative_register_labels_tile
from beta_product.annotations_per_tile_geo as atg
inner join beta_product.building_to_tile_aggr as rp on rp.tile_id = atg.tile_id;


select 
	sum(case when annotated_label = 1 and register_label = 0 then 1 else 0 end) as false_negatives,
	sum(case when annotated_label = 0 and register_label = 1 then 1 else 0 end) as false_positives
from beta_product.diff_register_annotations
where register_label <> annotated_label;


drop table if exists beta_product.diff_register_predictions_annotations;
create table beta_product.diff_register_predictions_annotations
as
select 
	da.tile_id, da.tile_geom, 
	da.total_annotations, da.num_positives, da.num_negatives, da.default_annotation,
	da.area_id, da.uuid, 
	da.annotated_label as annotated_label,
	da.predicted_label as predicted_label,
	ra.register_label as register_label, 
	ra.weighted_number_register_labels_tile,
	ra.num_solar_panels_register_tile, 
	ra.num_negative_register_labels_tile
from beta_product.diff_predictions_annotations as da
inner join beta_product.diff_register_annotations as ra on ra.tile_id = da.tile_id;

select 
	count(*), 
	sum(annotated_label) as annotated_sp, 
	sum(predicted_label) as predicted_sp,
	sum(register_label) as register_sp
from beta_product.diff_register_predictions_annotations
where tile_id in (
select tile_id from beta_product.neighbourhood_to_tile
where bu_naam = 'De Hemelder'
);

-- 53150
select count(distinct(tile_id)) from beta_product.diff_register_predictions_annotations;

-- register, predictions, annotations for the annotations tiles per neighbourhood.
-- NOTE: number of register labels correct? should be number of tiles with negatives/positives too
drop table if exists beta_product.diff_register_predictions_annotations_nb;
create table beta_product.diff_register_predictions_annotations_nb
as
select 
	bu_code,
	bu_naam,
	wk_code,
	gm_naam,
	wkb_geometry,
	weighted_tiles_neighbourhood,
	
	num_solar_panels_annotations,
	num_negative_annotations,
	num_solar_panels_annotations / weighted_tiles_neighbourhood as perc_solar_panel_annotations, 
	num_negative_annotations / weighted_tiles_neighbourhood as perc_negative_annotations,
	
	num_solar_panel_predictions,
	num_negative_predictions,
	num_solar_panel_predictions / weighted_tiles_neighbourhood as perc_solar_panel_predictions,
	num_negative_predictions / weighted_tiles_neighbourhood as perc_negative_predictions,
	
	num_register_labels,
	num_solar_panels_register,	
	num_negative_register_labels,	
	num_solar_panels_register / num_register_labels as perc_solar_panel_register_labels,
	num_negative_register_labels / num_register_labels as perc_negative_register_labels	
from 
(
select 
	nt.bu_code,
	nt.bu_naam,
	nt.wk_code,
	nt.gm_naam,
	nt.wkb_geometry,
	sum(coalesce(area_fraction_in_neighbourhood, 0)) as weighted_tiles_neighbourhood,
	sum(
		case 
			when annotated_label != 1 then 0
			when annotated_label = 1 then 1 * area_fraction_in_neighbourhood		
		end
	)  as num_solar_panels_annotations,
	sum(
		case 
			when annotated_label != 0 then 0
			when annotated_label = 0 then 1 * area_fraction_in_neighbourhood	
		end
	) as num_negative_annotations,	
	
	
	sum(coalesce(area_fraction_in_neighbourhood, 0)) as weighter_number_of_annotations,
	sum(
		case 
			when predicted_label != 1 then 0
			when predicted_label = 1 then 1 * area_fraction_in_neighbourhood		
		end
	)  as num_solar_panel_predictions,
	sum(
		case 
			when predicted_label != 0 then 0
			when predicted_label = 0 then 1 * area_fraction_in_neighbourhood	
		end
	) as num_negative_predictions,
	
	sum(num_solar_panels_register_tile * area_fraction_in_neighbourhood) as num_solar_panels_register,	
	sum(num_negative_register_labels_tile * area_fraction_in_neighbourhood) as num_negative_register_labels,
	sum(weighted_number_register_labels_tile * area_fraction_in_neighbourhood) as num_register_labels
	
from beta_product.diff_register_predictions_annotations as dr
inner join beta_product.neighbourhood_to_tile as nt on nt.tile_id = dr.tile_id
group by nt.bu_code, nt.bu_naam, nt.wk_code, nt.gm_naam, nt.wkb_geometry
) s;


drop table if exists beta_product.weighted_diff_per_nb;
create table beta_product.weighted_diff_per_nb
as
select wpa.bu_code, wpa.bu_naam, wpa.wk_code, wpa.gm_naam,
        waa.num_solar_panel_annotations,
		waa.perc_solar_panel_annotations_ann_tiles,
        num_predictions,
        num_solar_panels_predictions,
        case when weighted_num_predictions > 0 then
            num_solar_panels_predictions / weighted_num_predictions
            else 0
        end as perc_solar_panel_predictions,
		waa.perc_solar_panel_predictions_ann_tiles,
        num_register_labels,
        num_solar_panels_register, 
        case when wra.weighted_tiles_neighbourhood > 0 then
            num_solar_panels_register / wra.weighted_tiles_neighbourhood
            else 0
        end as perc_solar_panels_register,
		waa.perc_solar_panel_register_ann_tiles,
        num_solar_panels_register / ST_Area(wpa.wkb_geometry) as num_solar_panels_register_norm,
        waa.num_solar_panel_predictions - waa.num_solar_panel_annotations as predictions_annotations_diff,
        waa.num_solar_panel_register - waa.num_solar_panel_annotations as register_annotations_diff,
        num_solar_panels_register - num_solar_panels_predictions as register_predictions_diff,
        wpa.wkb_geometry
from beta_product.weighted_predictions_aggr_nb as wpa
--inner join beta_product.weighted_annotations_aggr_nb as waa on waa.bu_code = wpa.bu_code
inner join beta_product.diff_register_predictions_annotations_nb as waa on waa.bu_code = wpa.bu_code
inner join beta_product.weighted_register_labels_aggr_nb as wra on wra.bu_code = wpa.bu_code;

drop table if exists beta_product.weighted_diff_per_nb_selection;
create table beta_product.weighted_diff_per_nb_selection
as
select
    bu_code, bu_naam, wk_code, gm_naam,
    perc_solar_panel_annotations_ann_tiles,
    min(perc_solar_panel_annotations_ann_tiles) over() as min_perc_solar_panel_annotations_of_tiles,
    max(perc_solar_panel_annotations_ann_tiles) over() as max_perc_solar_panel_annotations_of_tiles,
    perc_solar_panel_predictions,
    min(perc_solar_panel_predictions) over() as min_perc_solar_panel_predictions,
    max(perc_solar_panel_predictions) over() as max_perc_solar_panel_predictions,
	perc_solar_panel_predictions_ann_tiles,
    min(perc_solar_panel_predictions_ann_tiles) over() as min_perc_solar_panel_predictions_ann_tiles,
    max(perc_solar_panel_predictions_ann_tiles) over() as max_perc_solar_panel_predictions_ann_tiles,
    perc_solar_panels_register,
    min(perc_solar_panel_register_ann_tiles) over() as min_perc_solar_panel_register_ann_tiles,
    max(perc_solar_panel_register_ann_tiles) over() as max_perc_solar_panel_register_ann_tiles,	
    perc_solar_panel_register_ann_tiles,    
	perc_solar_panel_predictions_ann_tiles - perc_solar_panel_annotations_ann_tiles as perc_diff_predictions_annotations,
	min(perc_solar_panel_predictions_ann_tiles - perc_solar_panel_annotations_ann_tiles) over() as min_perc_diff_predictions_annotations,
    max( perc_solar_panel_predictions_ann_tiles - perc_solar_panel_annotations_ann_tiles) over() as max_perc_diff_predictions_annotations,
    perc_solar_panel_predictions - perc_solar_panels_register as perc_diff_predictions_register,
    min(perc_solar_panel_predictions - perc_solar_panels_register) over() as min_perc_diff_predictions_register,
    max(perc_solar_panel_predictions - perc_solar_panels_register) over() as max_perc_diff_predictions_register,
    perc_solar_panel_annotations_ann_tiles - perc_solar_panel_register_ann_tiles as perc_diff_annotations_register,
    min(perc_solar_panel_annotations_ann_tiles - perc_solar_panel_register_ann_tiles) over() as min_perc_diff_annotations_register,
    max(perc_solar_panel_annotations_ann_tiles - perc_solar_panel_register_ann_tiles) over() as max_perc_diff_annotations_register,
    wkb_geometry
from beta_product.weighted_diff_per_nb
where num_predictions > 0;

CREATE INDEX "weighted_diff_per_nb_selection_wkb_geometry_index" ON beta_product.weighted_diff_per_nb_selection USING gist (wkb_geometry);



select * from beta_product.diff_register_predictions_annotations_nb;

select 
	sum(case when annotated_label = predicted_label then 1 else 0 end) as register_wrong,
	sum(case when annotated_label = register_label then 1 else 0 end) as predictions_wrong,
	sum(case when predicted_label = register_label then 1 else 0 end) as annotation_maybe_wrong
from beta_product.diff_register_predictions_annotations
where annotated_label <> predicted_label or annotated_label <> register_label;

-- correct labels
-- 53150 - 44971 = 8179 labels that don't have the same label for the annotations, predictions, and register.
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as correct_negatives,
	sum(case when annotated_label = 1 then 1 else 0 end) as correct_positives
from beta_product.diff_register_predictions_annotations
where annotated_label = predicted_label and annotated_label = register_label;

-- incorrect predictions
-- 3022
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as false_positives,
	sum(case when annotated_label = 1 then 1 else 0 end) as false_negatives
from beta_product.diff_register_predictions_annotations
where annotated_label <> predicted_label and annotated_label = register_label;

-- incorrect register labels
-- 4428
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as false_positives,
	sum(case when annotated_label = 1 then 1 else 0 end) as false_negatives
from beta_product.diff_register_predictions_annotations
where annotated_label = predicted_label and annotated_label <> register_label;

-- incorrect annotation labels?
-- 729
select 
	count(*),
	sum(case when annotated_label = 0 then 1 else 0 end) as false_positives,
	sum(case when annotated_label = 1 then 1 else 0 end) as false_negatives
from beta_product.diff_register_predictions_annotations
where annotated_label <> predicted_label and annotated_label <> register_label;

drop table if exists beta_product.tiles_register_wrong;
create table  beta_product.tiles_register_wrong;
as 
select 
	*
from beta_product.diff_register_predictions_annotations
where annotated_label = predicted_label and annotated_label <> register_label;

drop table if exists beta_product.tiles_predictions_wrong;
create table beta_product.tiles_predictions_wrong
as 
select 
	*
from beta_product.diff_register_predictions_annotations
where annotated_label <> predicted_label and annotated_label = register_label;

drop table if exists beta_product.tiles_annotations_maybe_wrong;
create table beta_product.tiles_annotations_maybe_wrong
as 
select 
	*
from beta_product.diff_register_predictions_annotations
where register_label = predicted_label and annotated_label <> register_label;




--Checks

-- Neighbourhoods
-- 412 neighbourhoods
select count(distinct(bu_code)) from beta_product.selected_neighbourhoods;

select count(distinct(bu_code)) from public.tiles as ti 
inner join beta_product.selected_neighbourhoods as sn on ST_Intersects(ti.area, sn.wkb_geometry)
inner join beta_product.selected_buildings as sb on ST_Intersects(sb.geovlak, ti.area)
where ti.area_id = 19 or ti.area_id = 23
-- 208 neighbourhoods
select count(*) from beta_product.weighted_predictions_aggr_nb;

--208
select count(*) from beta_product.weighted_diff_per_nb_selection;

-- Unique Tiles
-- 116666
select count(*) from beta_product.selected_tiles;
-- 116666
select count(distinct(tile_id)) from beta_product.selected_tiles;
-- 116666 unique tiles
select count(distinct(tile_id)) from beta_product.building_to_tile;
-- 116666
select count(*) from beta_product.model_predictions_geo;
-- 116666
select count(distinct(tile_id)) from beta_product.model_predictions_geo;
-- 116666
select count(distinct(tile_id)) from beta_product.weighted_predictions_per_nb;
-- 116666
select count(distinct(tile_id)) from beta_product.neighbourhood_to_tile_building;
-- Unique annotated tiles
-- in public, unfiltered: 62526 --> in beta_product, filtered: 53150
select count(*) from beta_product.annotations_per_tile_geo;
select count(distinct(tile_id)) from beta_product.annotations_per_tile_geo;


-- 121188
select count(*) from beta_product.neighbourhood_to_tile;
-- 121188
select count(*) from beta_product.weighted_predictions_per_nb;
-- 78824
select count(*) from public.annotations_per_tile;
select count(distinct(UUID)) from public.annotations_per_tile;

--348305
select count(*) from beta_product.register_label_per_building;
-- 291929
select count(*) from beta_product.building_to_tile;

-- 301039
select count(*) from beta_product.neighbourhood_to_tile_building;
select count(*) from beta_product.neighbourhood_to_tile_building;

-- 54881
select count(*) from beta_product.weighted_annotations_per_nb;
-- 53150
select count(distinct(tile_id)) from beta_product.weighted_annotations_per_nb;





-- Old Checks
select count(*) from beta_product.selected_neighbourhoods where in_train_validation_set = true;
select count(distinct(uuid)) from beta_product.annotations_per_tile_geo;
-- 116666 tiles, less than we have predictions
select count(distinct(tile_id)) from beta_product.neighbourhood_to_tile_building;
-- 208?
select count(*) from beta_product.weighted_register_labels_aggr_nb;
select count(*) from beta_product.weighted_predictions_aggr_nb;
-- 116666 
select count(distinct(tile_id)) from beta_product.weighted_predictions_per_nb;
-- 208
select count(distinct(bu_code)) from beta_product.weighted_predictions_per_nb;
-- 116666, same number as selected_tiles, for each of the selected_tiles we have a prediction
select count(*) from beta_product.model_predictions_geo;
-- 134379
select count(*) from beta_product.model_predictions;

-- 412
select count(*) from beta_product.weighted_annotations_aggr_nb;
-- 53150 unique tiles in annotation set vs. 116666 for the building set.
-- these 53150 are the tiles that intersect with buildings.
select count(distinct(tile_id)) from beta_product.annotations_per_tile_geo;

--count(*) = 347856 = count(distinct(identificatie)) = 347856, all buildings unique.
select count(*) from beta_product.selected_buildings;
select count(distinct(identificatie)) from beta_product.selected_buildings;

-- Create pv_2017_nl_new table
-- 569667
select count(*) from public.solarpanel_addresses_orig;
-- 544689
select count(*) from beta_product.pv_2017_nl_new;
-- 116666
select count(distinct(tile_id)) from beta_product.selected_tiles;
-- 116666
select count(distinct(tile_id)) from beta_product.neighbourhood_to_tile;
-- 116666
select count(distinct(tile_id)) from beta_product.building_to_tile;
-- 301039 with inner join, gets rid of
select count(*) from beta_product.neighbourhood_to_tile_building;

-- 116666
select count(distinct(tile_id)) from beta_product.neighbourhood_to_tile_building;

-- Important
-- 52 tiles with aggregated label > 1
select sum(num) from
(
    select 1 as num from beta_product.building_to_tile
    group by tile_id
    having sum(weighted_register_label_tile) > 1
) tiles_label_bigger_than_one;

select tile_id, sum(weighted_register_label_tile) as num from beta_product.building_to_tile
group by tile_id
having sum(weighted_register_label_tile) > 1;

-- still correct?
select
    bu_code, bu_naam,
    sum(tile_area_fraction_in_neighbourhood) as num_buildings
from beta_product.neighbourhood_to_tile_building
group by bu_code, bu_naam
having sum(tile_area_fraction_in_neighbourhood) < 30;

select count(*) from beta_product.weighted_diff_per_nb
where bu_code in
(
    select
        bu_code
    from beta_product.neighbourhood_to_tile_building
    group by bu_code, bu_naam
    having sum(tile_area_fraction_in_neighbourhood) < 30
);

select bu_naam, perc_diff_predictions_annotations, perc_diff_predictions_register, perc_diff_annotations_register from beta_product.weighted_diff_per_nb_selection
where bu_naam in ('Gravenrode', 'Brandenberg', 'Klinkerkwartier', 'Gracht');
