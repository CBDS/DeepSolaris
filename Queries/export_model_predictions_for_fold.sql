copy (
	select 
		df.dataset_id, df.fold_id, df.fold_name, dff.file_id, dft.fold_type_name, mp.model_id, mp.prediction, fa.label  
	from dataset_fold as df
	inner join dataset_fold_files as dff on dff.fold_id = df.fold_id
	inner join dataset_fold_types as dft on dft.fold_type_id = dff.fold_type_id
	inner join model_predictions as mp on mp.fold_id = dff.fold_id and mp.file_id = dff.file_id
	inner join files_annotations as fa on fa.file_id = dff.file_id
	where df.fold_id = 16
	order by df.dataset_id, df.fold_id, dff.file_id, dff.fold_type_id
) to '/tmp/heerlen-hr-beta-product-predictions.csv' csv header delimiter ';';




