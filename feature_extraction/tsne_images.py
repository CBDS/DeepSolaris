from sklearn.manifold import TSNE
from hdf5datasetwriter import HDF5DatasetWriter
import matplotlib.pyplot as plt
import argparse
import h5py
import os
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--db", required=True, help="path HDF5 database")
args = vars(parser.parse_args())

print("Loading the images")
#orig_images = np.load(args["image_file"])
labels = np.load(args["labels"])

print("Loading features")
db = h5py.File(args["db"], "r")
features = db["features"]

x_embedded = TSNE(n_components=2).fit_transform(features)

colors = np.array(["#00FF00", "#FF0000"])
plt.scatter(x_embedded[0], x_embedded[1], color=colors[labels])
