from tensorflow.keras.models import load_model, Model
from hdf5datasetwriter import HDF5DatasetWriter
from tensorflow.keras.applications.vgg16 import preprocess_input, VGG16
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import argparse
import pickle
import h5py
import os
import pandas as pd
import numpy as np
import progressbar


def get_image_generator(directory, label_file, input_shape, filename_column_name, label_column_name, delimiter):
    dataframe = pd.read_csv(label_file, sep=delimiter)
    dataframe[label_column_name] = dataframe[label_column_name].astype(str)
    image_data_generator = ImageDataGenerator(
        preprocessing_function=preprocess_input)
    return image_data_generator.flow_from_dataframe(dataframe=dataframe, directory=directory, x_col=filename_column_name, y_col=label_column_name, target_size=input_shape)


def get_features_for_class(db, data_key, label_key):
    i = int(db[label_key].shape[0] * 0.75)
    return db[data_key][:i], db[label_key][:i], db[data_key][i:], db[label_key][i:]


def write_features(model, batch_size, image_generator, feature_vector_size, output_filename):
    number_of_images = len(image_generator.filenames)
    dataset = HDF5DatasetWriter((number_of_images, feature_vector_size), output_filename, dataKey="features",
                                bufSize=1000)
    widgets = ["Extracting Features: ", progressbar.Percentage(
    ), " ", progressbar.Bar(), " ", progressbar.ETA()]
    pbar = progressbar.ProgressBar(maxval=len(images), widgets=widgets).start()
    for i, images_and_labels_for_batch in enumerate(image_generator):
        images_for_batch = images_and_labels_for_batch[0]
        batchLabels = images_and_labels_for_batch[1]

        features = model.predict(images_for_batch, batch_size=batch_size)
        features = features.reshape((features.shape[0], feature_vector_size))
        dataset.add(features, batchLabels)
        pbar.update(i)
    dataset.close()
    pbar.finish()


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--image-dataset", required=True,
                    help="The directorycontaining the images")
parser.add_argument("-l", "--labels", required=True,
                    help="The csv file with the to labels")
parser.add_argument("-m", "--model", required=True,
                    help="path to the model used to extract features")
parser.add_argument("-n", "--layer-name", default="block5_pool",
                    help="The name of the layer to use for feature extraction")
parser.add_argument("-b", "--batch-size", type=int, default=32,
                    help="batch size of images to be passed through network")
parser.add_argument("-d", "--db", required=True, help="path HDF5 database")
parser.add_argument("-f", "--filename-column-name", default="filename",
                    help="The column name in the label file containing the filenames")
parser.add_argument("-c", "--label-column-name", default="label",
                    help="The column name in the label file containing the labels")
parser.add_argument("-s", "--delimiter", default=";",
                    help="The delimiter for the label csv file")
args = parser.parse_args()

model = load_model(args.model)
last_layer = model.get_layer(name=args.layer_name)

input_shape = model.layers[0].input_shape[1:-1]
print(f"Model input shape: {input_shape}")
images = get_image_generator(
    args.image_dataset, args.labels, input_shape, args.filename_column_name, args.label_column_name, args.delimiter)

feature_vector_size = np.prod(last_layer.output_shape[1:])
print("Feature vector size: {}".format(feature_vector_size))
feature_model = Model(inputs=model.layers[0].input, outputs=last_layer.output)

if not os.path.exists(args.db):
    print("Extracting features from {}".format(args.image_dataset))
    write_features(feature_model, args.batch_size,
                   images, feature_vector_size, args.db)
