import csv
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", required=True, help="The csv file with the annotations")
parser.add_argument("-o", "--output", required=True, help="The output csv file containing the uuid and labels only")
parser.add_argument("-d", "--delimiter", default=";", help="The delimiter used to read the csv file")
args = vars(parser.parse_args())

delimiter = args["delimiter"]
with open(args["input"]) as input_file:
    csv_reader = csv.reader(input_file, delimiter=delimiter)
    with open(args["output"], "w") as output_file:
        csv_writer = csv.DictWriter(output_file, fieldnames=["uuid", "label"], delimiter=delimiter)
        csv_writer.writeheader()
        for row in csv_reader:
            csv_writer.writerow({"uuid": row[2], "label": row[-1]})
