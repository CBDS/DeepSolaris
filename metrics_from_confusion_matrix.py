from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--num-positives", type=int, required=True, help="The number of true positives")
parser.add_argument("-n", "--num-negatives", type=int, required=True, help="The number of true negatives")
parser.add_argument("-fp", "--num-false-positives", type=int, required=True, help="The number of false positives")
parser.add_argument("-fn", "--num-false-negatives", type=int, required=True, help="The number of false negatives")
args = vars(parser.parse_args())

true_positives = [1 for i in range(args["num_positives"] + args["num_false_negatives"])]
true_negatives = [0 for i in range(args["num_negatives"] + args["num_false_positives"])]
true_labels = np.array(true_positives + true_negatives)

predicted_positives = [1 for i in range(args["num_positives"])]
false_negatives = [0 for i in range(args["num_false_negatives"])]
predicted_negatives = [0 for i in range(args["num_negatives"])]
false_positives = [1 for i in range(args["num_false_positives"])]
predicted_labels = np.array(predicted_positives + false_negatives + predicted_negatives + false_positives)

print("Confusion matrix:")
print(confusion_matrix(true_labels, predicted_labels))
print("Classification report:")
print(classification_report(true_labels, predicted_labels))


