from sklearn.model_selection import RandomizedSearchCV
from cbds.deeplearning import Project, ImageGenerator
from cbds.deeplearning import ImageGenerator
from cbds.deeplearning.settings import RMSPropSettings
from cbds.deeplearning.models.vgg16 import vgg16
from cbds.deeplearning.metrics import ClassificationReportCallback, ConfusionMatrixCallback, PlotRocCallback, MetricCurveCallback
from scipy.stats import uniform
from keras.layers import Dense, Dropout, Flatten, GlobalAveragePooling2D
from keras.models import Model
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.applications.vgg16 import preprocess_input
from keras.utils import to_categorical
from keras import regularizers
from itertools import product
import os
import argparse


def create_vgg16_model(input_shape, layer_index=15):
    base_model = vgg16(input_shape, include_top=False, batch_normalization=False)
    for layer in base_model.layers[:layer_index]:
        layer.trainable = False
    for layer in base_model.layers[layer_index:]:
        layer.trainable = True
    x = Flatten()(base_model.output)
    predictions = Dense(2, activation="softmax")(x)
    model_name = "vgg16_full_fc1_aug_frozen_{}_softmax".format(layer_index)
    return model_name, Model(base_model.input, predictions)

def get_datasets(project):
    filenames = os.listdir(project.datasets_path)
    print(project.datasets_path)
    for filename in filenames:
        if "_training" in filename:
            training_set = project.dataset(filename)
        elif "_test" in filename:
            test_set = project.dataset(filename)
        elif "_validation" in filename:
            validation_set = project.dataset(filename)

    return training_set, test_set, validation_set


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--project-path", default=r"/media/Data/Work/CBS/DeepSolaris", help="The path where the project file should be stored")
    parser.add_argument("-e", "--epochs", default=10, type=int, help="The number of epochs to run")
    parser.add_argument("-b", "--batch-size", default=64, type=int, help="The batch-size for the experiment")
    parser.add_argument("-lr", "--learning-rate", default=1e-5, type=float, help="The learning rate to use")
    parser.add_argument("-f", "--frozen-index", default=7, type=int, help="The layer index until which layers are frozen")
    args = parser.parse_args()

    with Project(project_path=args.project_path) as project:
        training_set, validation_set, test_set = get_datasets(project)
        
        class_weights = training_set.class_weights

        training_set.labels = to_categorical(training_set.labels)
        validation_set.labels = to_categorical(validation_set.labels)
        test_set.labels = to_categorical(test_set.labels)

        train_generator = ImageGenerator(training_set)\
                          .with_seed(42)\
                          .with_rotation_range(30)\
                          .with_width_shift_range(0.1)\
                          .with_height_shift_range(0.1)\
                          .with_zoom_range(0.2)\
                          .with_shear_range(0.2)\
                          .with_horizontal_flip(True)\
                          .with_fill_mode("reflect")

        validation_generator = ImageGenerator(validation_set)\
                         .with_seed(84)

        image_shape = training_set.data[0].shape
        name, cnn_model = create_vgg16_model(input_shape=image_shape, layer_index=args.frozen_index)

        model = project.model(name)
        model.create_model(cnn_model)
        model.plot()
        cnn_model.summary()

        with model.run().with_epochs(args.epochs)\
                .with_batch_size(args.batch_size)\
                .with_loss_function("binary_crossentropy")\
                .with_optimizer(RMSPropSettings(lr=args.learning_rate))\
                .with_metric_callbacks([ClassificationReportCallback(), ConfusionMatrixCallback(), MetricCurveCallback(), PlotRocCallback()])\
                .with_class_weights(class_weights)\
                .with_train_dataset(train_generator)\
                .with_test_dataset(validation_generator)\
                .with_evaluation_datasets([validation_set, test_set]) as run:
                run.train()
                run.evaluate()


if __name__ == "__main__":
    main()
