from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score, classification_report
import matplotlib.pyplot as plt
import numpy as np
import csv
import argparse

def prediction_labels_for(predictions, cut_off):
    return predictions >= cut_off

def metric_curve(true_labels, predictions, cut_offs, metric):
    return np.array([metric(true_labels, prediction_labels_for(predictions, cut_off)) for cut_off in cut_offs])

def plot_metric_curve(axes, metric_name, metric, true_labels, predictions, cut_offs, label, plot_color, cut_off_color="g"):
    metric_values = metric_curve(true_labels, predictions, cut_offs, metric)
    axes.set_title(metric_name)
    axes.set_ylim([0, 1])
    axes.set_xlim([0, 1])
    axes.plot(cut_offs, metric_values, color=plot_color, label=label)

    max_value_index = metric_values.argmax()
    best_cut_off = cut_offs[max_value_index]
    axes.axvline(best_cut_off, ymax=metric_values[max_value_index], color=cut_off_color, label="{}_cut_off".format(label))
    return best_cut_off

def get_predictions_and_labels(filename, delimiter):
    true_labels = []
    predictions = []
    with open(filename) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=delimiter)
        for row in csv_reader:
            true_labels.append(int(row["label"]))
            predictions.append(float(row["prediction"]))

    return np.array(true_labels), np.array(predictions)

def classification_reports(cut_off, true_labels, predictions, validation_set, val_true_labels, val_predictions):
    print("Classification reports for cut-off {}".format(cut_off))
    print("Training set")
    print(classification_report(true_labels, predictions >= cut_off))

    if validation_set:
        print("Validation set")
        print(classification_report(val_true_labels, val_predictions >= cut_off))

parser = argparse.ArgumentParser()
parser.add_argument("-i","--input", required=True, help="The csv file with the true labels and the predictions")
parser.add_argument("-v","--validation-file", default=None, help="A csv file with the true labels and the predictions of the validation file")
parser.add_argument("-o","--output-file", required=True, help="The name of the image file to save the plot")
parser.add_argument("-d", "--delimiter", default=";", help="The delimiter for the csv file")
parser.add_argument("-n", "--number-of-cut-offs", default=1000, help="The number of steps between 0 and 1 for the cut off values")
args = vars(parser.parse_args())

filename = args["input"]
validation_filename = args["validation_file"]
delimiter = args["delimiter"]
number_of_cut_offs = args["number_of_cut_offs"]

true_labels, predictions = get_predictions_and_labels(filename, delimiter)
val_true_labels = []
val_predictions = []
if validation_filename is not None:
    val_true_labels, val_predictions = get_predictions_and_labels(validation_filename, delimiter)

metrics  = {"precision": precision_score, "recall": recall_score, "f1": f1_score, "accuracy": accuracy_score}
cut_offs = np.linspace(0, 1, num=number_of_cut_offs)

_, ax = plt.subplots(2, 2)
for index, (metric_name, metric) in enumerate(metrics.items()):
    r = int(index <= 1)
    c = int(index % 2 == 0)
    cut_off = plot_metric_curve(ax[r, c], metric_name, metric, true_labels, predictions, cut_offs, "training set", "b")
    if validation_filename is not None:
        val_cut_off = plot_metric_curve(ax[r, c], metric_name, metric, val_true_labels, val_predictions, cut_offs, "validation set", "r", "y")
    if metric_name == "f1":
        best_cut_off = cut_off
plt.legend()


has_validation_set = validation_filename is not None
classification_reports(0.5, true_labels, predictions, has_validation_set, val_true_labels, val_predictions)
classification_reports(best_cut_off, true_labels, predictions, has_validation_set, val_true_labels, val_predictions)

plt.savefig(args["output_file"])
plt.show()
