import csv
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--left-file", required=True, help="One of the files to join")
parser.add_argument("-r", "--right-file", required=True, help="The other file to join")
parser.add_argument("-o", "--output-file", required=True, help="The output file to store the joined results")
parser.add_argument("-k", "--key", default="uuid", help="The key/column to join the both files on")
parser.add_argument("-d", "--delimiter", default=";", help="The delimiter for the csv files")
args = vars(parser.parse_args())

def get_rows(filename, key, delimiter):
    with open(filename) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=delimiter)
        return {row[key]: row for row in csv_reader}

def join_rows(left_rows, right_rows, key):
    common_keys = set(left_rows.keys()) & set(right_rows.keys())
    column_names = ["uuid", "prediction", "label"]
    return column_names, [{"uuid": key, "prediction": left_rows[key]["prediction"], "label": right_rows[key]["label"]}  for key in common_keys]


key = args["key"]
delimiter = args["delimiter"]

left_rows = get_rows(args["left_file"], key, delimiter)
right_rows = get_rows(args["right_file"], key, delimiter)
column_names, joined_rows = join_rows(left_rows, right_rows, key)

with open(args["output_file"], "w") as joined_file:
    csv_writer = csv.DictWriter(joined_file, fieldnames=column_names, delimiter=delimiter)
    csv_writer.writeheader()
    for row in joined_rows:
        csv_writer.writerow(row)
