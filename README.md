# Introduction

This repository contains the Python code used in the EuroStat DeepSolaris project. The code was developed by the Center of Big Data Statistics of Statistics Netherlands and is released under an open source MIT licence.

# Installation

First, to use the code in this repository, clone it using git (see the clone button on the top):

```bash
$ git clone https://gitlab.com/CBDS/DeepSolaris.git
```

or download the source code [here](../-/archive/master/DeepSolaris-master.zip) and unzip this file. Go to the directory where you cloned or unzipped the code.

```bash
~$ cd /path/to/DeepSolaris/
/path/to/DeepSolaris$
```

To use the code in this repository it is best to create a Python virtual environment containing all the necessary Python packages.
To install Python virtual environments the following tutorial can be followed [Installing Python Virtual Environments (Ubuntu 18.04)](https://itnext.io/virtualenv-with-virtualenvwrapper-on-ubuntu-18-04-goran-aviani-d7b712d906d5). After that create a virtual environment with the following command:

```bash
/path/to/DeepSolaris$ mkvirtualenv DeepSolaris
```

To use this virtual environment, use the following command:

```bash
/path/to/DeepSolaris$ workon DeepSolaris
```

If the command worked, (DeepSolaris) appears before your prompt. Check that your prompt looks like this:


```bash
(DeepSolaris) /path/to/DeepSolaris$
```

To install the necessary packages in the DeepSolaris virtual environment you can now run the following command (if you do not own a GPU):


```bash
(DeepSolaris) /path/to/DeepSolaris$ pip install -r install.txt
```

For GPU support make sure you have the correct Nvidia drivers and CUDA libraries installed before proceeding. This is outside the scope of this README but a good tutorial can be found here [PyImageSearch tutorial: install tensorflow and keras for deep learning](https://www.pyimagesearch.com/2019/01/30/ubuntu-18-04-install-tensorflow-and-keras-for-deep-learning/). Follow this tutorial until just before step #5. Step #5 creates a virtual environment like we just did and Step #6 will be dealt with by the install-gpu.txt

Again make sure you are in the DeepSolaris virtual environment, otherwise the packages will be installed system wide. If you want to install the DeepSolaris so that it has GPU support, run the following command:

```bash
(DeepSolaris) /path/to/DeepSolaris$ pip install -r install-gpu.txt
```

# Overview
This repository is structured as follows:

- The [Queries](./Queries) directory contains the SQL scripts used on a PostGIS database to generate the tables used in this project.
- The [annotations](./annotations) directory contains the Python scripts that create numpy files with the images and the labels on the basis of a csv file with the annotations and the directories containing the image files (tiles) for each dataset.
- The [feature\_extraction](./feature_extraction) contains the code used to extract feature vectors from the images from a certain layer of the Deep Learning network. These feature vectors are stored in a hdf5 file which can be used later to train a logistic regression or SVM classifier. 
- The [model\_training](./model_training) directory contains the script used to fine-tune the VGG16 based networks on the Heerlen data.
- The [notebooks](./notebooks) directory contains jupyter notebooks with some explorative studies.

# Acknowledgements

This research was conducted under the ESS action 'Merging Geostatistics and Geospatial Information in Member States' (grant agreement no.: 08143.2017.001-2017.408) and an investment of Statistics Netherlands for the development of Deep Learning models, practices, and methodology.
